object PropertiesForm: TPropertiesForm
  Left = 326
  Top = 167
  BorderStyle = bsDialog
  Caption = 'Pole properties'
  ClientHeight = 351
  ClientWidth = 177
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefault
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox2: TGroupBox
    Left = 0
    Top = 0
    Width = 177
    Height = 137
    Caption = 'Pole'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object Label3: TLabel
      Left = 6
      Top = 70
      Width = 60
      Height = 13
      Caption = 'Bragg angle,'
    end
    object Label2: TLabel
      Left = 6
      Top = 40
      Width = 90
      Height = 13
      Caption = 'Angle to main pole,'
    end
    object Label4: TLabel
      Left = 6
      Top = 96
      Width = 93
      Height = 13
      Caption = 'Interplane distance,'
    end
    object Label5: TLabel
      Left = 21
      Top = 112
      Width = 49
      Height = 13
      Caption = 'Angstroms'
    end
    object Label10: TLabel
      Left = 104
      Top = 40
      Width = 68
      Height = 13
      AutoSize = False
      Caption = 'Label10'
      WordWrap = True
    end
    object Label11: TLabel
      Left = 104
      Top = 70
      Width = 68
      Height = 13
      AutoSize = False
      Caption = 'Label11'
      WordWrap = True
    end
    object Label12: TLabel
      Left = 104
      Top = 104
      Width = 68
      Height = 13
      AutoSize = False
      Caption = 'Label12'
      WordWrap = True
    end
    object Label9: TLabel
      Left = 72
      Top = 17
      Width = 41
      Height = 16
      Caption = 'Label9'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
    object Label1: TLabel
      Left = 16
      Top = 17
      Width = 47
      Height = 16
      Caption = 'indexes'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label13: TLabel
      Left = 97
      Top = 38
      Width = 5
      Height = 15
      Caption = #176
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Symbol'
      Font.Style = []
      ParentFont = False
    end
    object Label14: TLabel
      Left = 67
      Top = 68
      Width = 5
      Height = 15
      Caption = #176
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Symbol'
      Font.Style = []
      ParentFont = False
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 137
    Width = 177
    Height = 177
    Caption = 'Extended'
    TabOrder = 0
    object Label6: TLabel
      Left = 8
      Top = 24
      Width = 89
      Height = 13
      Caption = 'Allowed poles here'
    end
    object Label7: TLabel
      Left = 8
      Top = 68
      Width = 37
      Height = 13
      Caption = 'Material'
    end
    object Label8: TLabel
      Left = 8
      Top = 120
      Width = 61
      Height = 13
      Caption = 'Wave length'
    end
    object ComboBox1: TComboBox
      Left = 8
      Top = 85
      Width = 81
      Height = 21
      ItemHeight = 13
      TabOrder = 0
      Text = 'Si'
      OnChange = ComboBox1Change
      OnKeyPress = ComboBox2KeyPress
      Items.Strings = (
        'Si'
        'Ge'
        'Ga As'
        'Ga P'
        'In As'
        'Al As'
        'In P'
        'Al P')
    end
    object ComboBox2: TComboBox
      Left = 8
      Top = 40
      Width = 97
      Height = 21
      ItemHeight = 13
      TabOrder = 1
      Text = 'Polus'
      OnChange = ComboBox2Change
      OnKeyPress = ComboBox2KeyPress
    end
    object Edit1: TEdit
      Left = 8
      Top = 136
      Width = 81
      Height = 21
      TabOrder = 2
      Text = '1,54'
      OnKeyPress = Edit1KeyPress
    end
    object RadioButton1: TRadioButton
      Left = 96
      Top = 131
      Width = 65
      Height = 17
      Caption = 'Angstrom'
      Checked = True
      TabOrder = 3
      TabStop = True
      OnClick = RadioButton1Click
    end
    object RadioButton2: TRadioButton
      Left = 96
      Top = 147
      Width = 41
      Height = 17
      Caption = 'nm'
      TabOrder = 4
      OnClick = RadioButton1Click
    end
  end
  object CheckBoxSendToHiddenOpt: TCheckBox
    Left = 8
    Top = 318
    Width = 137
    Height = 17
    Caption = 'Move to tabs'
    TabOrder = 2
    OnClick = CheckBoxSendToHiddenOptClick
  end
  object CheckBoxOptOnTop: TCheckBox
    Left = 8
    Top = 335
    Width = 103
    Height = 14
    Caption = 'Always on top'
    TabOrder = 3
    OnClick = CheckBoxOptOnTopClick
  end
end
