object AngleForm: TAngleForm
  Left = 457
  Top = 244
  BorderStyle = bsDialog
  Caption = 'Angle'
  ClientHeight = 235
  ClientWidth = 177
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object LabelAngle: TLabel
    Left = 24
    Top = 152
    Width = 71
    Height = 13
    Caption = 'Angle, degrees'
  end
  object LabelAngResult: TLabel
    Left = 24
    Top = 168
    Width = 75
    Height = 13
    Caption = 'LabelAngResult'
  end
  object SpinEditVec1_X: TSpinEdit
    Left = 28
    Top = 30
    Width = 41
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 0
    Value = 0
  end
  object SpinEditVec1_Y: TSpinEdit
    Left = 68
    Top = 30
    Width = 41
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 1
    Value = 0
  end
  object SpinEditVec1_Z: TSpinEdit
    Left = 108
    Top = 30
    Width = 41
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 2
    Value = 0
  end
  object SpinEditVec2_X: TSpinEdit
    Left = 27
    Top = 79
    Width = 41
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 3
    Value = 0
  end
  object SpinEditVec2_Y: TSpinEdit
    Left = 67
    Top = 79
    Width = 41
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 4
    Value = 0
  end
  object SpinEditVec2_Z: TSpinEdit
    Left = 107
    Top = 79
    Width = 41
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 5
    Value = 0
  end
  object StaticTextVec1: TStaticText
    Left = 58
    Top = 10
    Width = 53
    Height = 20
    Caption = 'Vector 1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
  end
  object StaticTextVec2: TStaticText
    Left = 58
    Top = 59
    Width = 53
    Height = 20
    BevelInner = bvLowered
    Caption = 'Vector 2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 7
  end
  object ButtonAngCulc: TButton
    Left = 27
    Top = 114
    Width = 120
    Height = 23
    Caption = 'Calculate'
    TabOrder = 8
    OnClick = ButtonAngCulcClick
  end
  object CheckBoxSendToHiddenAng: TCheckBox
    Left = 21
    Top = 200
    Width = 136
    Height = 17
    Caption = 'Move to tabs'
    TabOrder = 9
    OnClick = CheckBoxSendToHiddenAngClick
  end
  object CheckBoxAngOnTop: TCheckBox
    Left = 21
    Top = 218
    Width = 103
    Height = 14
    Caption = 'Always on top'
    TabOrder = 10
    OnClick = CheckBoxAngOnTopClick
  end
end
