unit resol;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Spin, ExtCtrls;

type
  TFormResolution = class(TForm)
    StaticText1: TStaticText;
    ButtonResolOk: TButton;
    GroupBoxResolution: TGroupBox;
    ComboBoxSquareSize: TComboBox;
    ComboBoxDpi: TComboBox;
    StaticTextSquareLength: TStaticText;
    StaticTextDpi: TStaticText;
    StaticTextEdenic: TStaticText;
    RadioButtonPixels: TRadioButton;
    RadioButtonMm: TRadioButton;
    CheckBoxCurSize: TCheckBox;
    ButtonResolCancel: TButton;
    procedure ButtonResolOkClick(Sender: TObject);
   // procedure ComboBoxResolChange(Sender: TObject);
    procedure RadioButtonMmClick(Sender: TObject);
    procedure RadioButtonPixelsClick(Sender: TObject);
    procedure CheckBoxCurSizeClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ButtonResolCancelClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormResolution: TFormResolution;

implementation

{$R *.dfm}

procedure TFormResolution.ButtonResolOkClick(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

procedure TFormResolution.RadioButtonMmClick(Sender: TObject);
begin
  ComboBoxDpi.Enabled:=true;
  ComboBoxSquareSize.Items.Clear;
  ComboBoxSquareSize.Items.Add('50');
  ComboBoxSquareSize.Items.Add('70');
  ComboBoxSquareSize.Items.Add('100');
  ComboBoxSquareSize.Items.Add('200');
  ComboBoxSquareSize.Text:='200';
end;

procedure TFormResolution.RadioButtonPixelsClick(Sender: TObject);
begin
  ComboBoxDpi.Enabled:=false;
  ComboBoxSquareSize.Items.Clear;
  ComboBoxSquareSize.Items.Add('240');
  ComboBoxSquareSize.Items.Add('320');
  ComboBoxSquareSize.Items.Add('480');
  ComboBoxSquareSize.Items.Add('640');
  ComboBoxSquareSize.Items.Add('768');
  ComboBoxSquareSize.Items.Add('800');
  ComboBoxSquareSize.Items.Add('1024');
  ComboBoxSquareSize.Items.Add('1152');
  ComboBoxSquareSize.Items.Add('1200');
  ComboBoxSquareSize.Items.Add('1280');
  ComboBoxSquareSize.Items.Add('1600');
  ComboBoxSquareSize.Text:='1200';
end;

procedure TFormResolution.CheckBoxCurSizeClick(Sender: TObject);
begin
  if CheckBoxCurSize.Checked then
  begin
    //GroupBoxResolution.Enabled:=false;
    ComboBoxSquareSize.Enabled:=false;
  end
  else
    ComboBoxSquareSize.Enabled:=true;
end;

procedure TFormResolution.FormCreate(Sender: TObject);
begin
  Left:=(Screen.Width - Width) div 2;
  Top:=(Screen.Height - Height) div 2;
end;

procedure TFormResolution.ButtonResolCancelClick(Sender: TObject);
begin
  Close;
end;

end.
