unit filesmyx;

interface

uses SysUtils;

function GetFileNameOnly(PathAndFname: string): string;

implementation

function GetFileNameOnly(PathAndFname: string): string;
var
  LengthOfFName,i,j: Word;
  FName: string;
begin
  FName:='';
  LengthOfFName:=StrLen(PChar(PathAndFname));
  j:=0;
  for i:=LengthOfFName downto 1 do
  begin
    if PathAndFname[i]<>'\' then
    begin
      FName:=FName+PathAndFname[i];
      Inc(j);
    end
    else
      break;
  end;
  for i:=j downto 1 do
    Result:=Result+FName[i];
end;


end.
 