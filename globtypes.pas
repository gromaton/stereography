unit globtypes;

interface

uses Graphics,IniFiles;

type Vector = Array [1..4] of Extended; //������� ���� Vector-������������
type Matrix = Array [1..3,1..3] of Extended; //������� ���� Matrix
type Direct = Array [1..3] of SmallInt; //������ ����� ��������
type SomeDir = Array [0..48] of Vector;
type ManyVectors = Array [1..10520] of Vector;
type VeryManyVectors = Array of Vector;
type Polar = Array [1..2] of Single;

Var
  LgSett: TIniFile;
  Stepr: TBitMap;
  P,oldP,G,AllotVecOpt,AllotVecAng,AllotVecAng2,RepaintVec: Vector;
  spScale,spScaleOld: SmallInt;  //�������
  Count: SmallInt;
  TBChange, Stopbit, EnableAdditionalDraw,
  OptionFormEnabled,AngleFormEnabled, RepaintAllot: Boolean;
  TempFile: TextFile;
  J,OldJ: Direct;
  PixelsX,PixelsY: integer;
  MaxIndex: Byte;

//��������� ���������� ����������
  dRtmp: Word; //����������� ���������� ����������� �� ����� �������

implementation

end.
