object FormResolution: TFormResolution
  Left = 537
  Top = 162
  BorderStyle = bsDialog
  Caption = 'Image settings'
  ClientHeight = 155
  ClientWidth = 302
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefault
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object StaticText1: TStaticText
    Left = 72
    Top = 8
    Width = 117
    Height = 20
    Caption = 'Resolution settings'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object ButtonResolOk: TButton
    Left = 230
    Top = 53
    Width = 61
    Height = 20
    Caption = 'Ok'
    TabOrder = 1
    OnClick = ButtonResolOkClick
  end
  object GroupBoxResolution: TGroupBox
    Left = 16
    Top = 32
    Width = 201
    Height = 89
    Caption = 'Resolution'
    TabOrder = 2
    object ComboBoxSquareSize: TComboBox
      Left = 115
      Top = 14
      Width = 81
      Height = 21
      ItemHeight = 13
      TabOrder = 0
      Text = '1200'
      Items.Strings = (
        '240'
        '320'
        '640'
        '480'
        '600'
        '800'
        '768'
        '1024'
        '1152'
        '1200'
        '1280'
        '1600')
    end
    object ComboBoxDpi: TComboBox
      Left = 115
      Top = 38
      Width = 81
      Height = 21
      Enabled = False
      ItemHeight = 13
      TabOrder = 1
      Text = '300'
      Items.Strings = (
        '72'
        '300'
        '600')
    end
    object StaticTextSquareLength: TStaticText
      Left = 22
      Top = 16
      Width = 84
      Height = 17
      Caption = 'Square side size:'
      TabOrder = 2
    end
    object StaticTextDpi: TStaticText
      Left = 84
      Top = 40
      Width = 23
      Height = 17
      Caption = 'Dpi:'
      TabOrder = 3
    end
    object StaticTextEdenic: TStaticText
      Left = 8
      Top = 64
      Width = 31
      Height = 17
      Caption = 'Units:'
      TabOrder = 4
    end
    object RadioButtonPixels: TRadioButton
      Left = 72
      Top = 64
      Width = 65
      Height = 17
      Caption = 'Pixels'
      Checked = True
      TabOrder = 5
      TabStop = True
      OnClick = RadioButtonPixelsClick
    end
    object RadioButtonMm: TRadioButton
      Left = 152
      Top = 64
      Width = 41
      Height = 17
      Caption = 'mm'
      TabOrder = 6
      OnClick = RadioButtonMmClick
    end
  end
  object CheckBoxCurSize: TCheckBox
    Left = 16
    Top = 128
    Width = 153
    Height = 17
    Caption = 'Take current size'
    TabOrder = 3
    OnClick = CheckBoxCurSizeClick
  end
  object ButtonResolCancel: TButton
    Left = 230
    Top = 85
    Width = 61
    Height = 20
    Cancel = True
    Caption = 'Cancel'
    TabOrder = 4
    OnClick = ButtonResolCancelClick
  end
end
