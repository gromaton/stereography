unit Getangle;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Spin, Addfunc, Solve2, globtypes, ComCtrls, ExtCtrls;

type
  TAngleForm = class(TForm)
    SpinEditVec1_X: TSpinEdit;
    SpinEditVec1_Y: TSpinEdit;
    SpinEditVec1_Z: TSpinEdit;
    SpinEditVec2_X: TSpinEdit;
    SpinEditVec2_Y: TSpinEdit;
    SpinEditVec2_Z: TSpinEdit;
    StaticTextVec1: TStaticText;
    StaticTextVec2: TStaticText;
    ButtonAngCulc: TButton;
    CheckBoxSendToHiddenAng: TCheckBox;
    CheckBoxAngOnTop: TCheckBox;
    LabelAngle: TLabel;
    LabelAngResult: TLabel;
    procedure ButtonAngCulcClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure CheckBoxSendToHiddenAngClick(Sender: TObject);
    procedure WMNCHitTest(var M: TWMNCHitTest); message wm_NCHitTest;
    procedure WMLButtonDown(var Message: Tmessage); message WM_NCLBUTTONDOWN;
    procedure FormCreate(Sender: TObject);
    procedure CheckBoxAngOnTopClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  AngleForm: TAngleForm;
  CurOnCaptionAng: Boolean;
implementation

uses mainster, options;

{$R *.dfm}

procedure TAngleForm.WMNCHitTest(var M: TWMNCHitTest);
begin
  inherited;
  if AngleForm.CheckBoxSendToHiddenAng.Checked then
  begin
    if M.Result = htCaption then CurOnCaptionAng:=true else CurOnCaptionAng:=false;
  end;
end;

procedure TAngleForm.WMLButtonDown(var Message: Tmessage);
begin
  if AngleForm.CheckBoxSendToHiddenAng.Checked then
  begin
    if CurOnCaptionAng=false then inherited else AngleForm.SetFocus;
  end
  else inherited;
end;

procedure TAngleForm.ButtonAngCulcClick(Sender: TObject);
var
  A,B: Vector;
  Res: Extended;
begin
  if ((SpinEditVec1_X.Value=0)and(SpinEditVec1_Y.Value=0)and(SpinEditVec1_Z.Value=0))
     or((SpinEditVec2_X.Value=0)and(SpinEditVec2_Y.Value=0)and(SpinEditVec2_Z.Value=0))
  then  exit;
  A:=GetMainVector(SpinEditVec1_X.Value,SpinEditVec1_Y.Value,SpinEditVec1_Z.Value);
  B:=GetMainVector(SpinEditVec2_X.Value,SpinEditVec2_Y.Value,SpinEditVec2_Z.Value);
  Res:=AngleBtwnVectors(A,B);
  LabelAngResult.Caption:=FloatToStr(Res);
end;

procedure TAngleForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  (*if CheckBoxSendToHiddenAng.Checked then TabSheetHiddenAng.Free;
  if (OptionForm.Visible=true){or(OptionForm.CheckBoxSendToHiddenOpt.Checked)} then Form1.SpeedButtonModeOption.Down:=true
  else Form1.SpeedButtonModeSimple.Down:=true;
  {if (CmprVectors(AllotVecOpt,AllotVecAng))and(not Form1.SpeedButtonModeSimple.Down)
    then SetAllotation(AllotVecAng,clBlue,clBlue)
  else
    if Mdl(AllotVecAng)<>0 then SetAllotation(AllotVecAng,clRed,clRed);}
  if Form1.SpeedButtonModeOption.Down then
  begin
    //Form1.PageControl1.ActivePage:=TabSheetHiddenOpt;
    OptionForm.Show;
  end;
  if not IsNullVector(AllotVecAng) then SetAllotation(AllotVecAng,clRed,clRed);
  if not IsNullVector(AllotVecAng2) then SetAllotation(AllotVecAng2,clRed,clRed);
  if Form1.SpeedButtonModeOption.Down then
    if (CmprVectors(AllotVecOpt,AllotVecAng) or CmprVectors(AllotVecOpt,AllotVecAng2)) then
      SetAllotation(AllotVecOpt,clBlue,clBlue);
  AllotVecAng[1]:=0; AllotVecAng[2]:=0; AllotVecAng[3]:=0;
  AllotVecAng2[1]:=0; AllotVecAng2[2]:=0; AllotVecAng2[3]:=0;
  G[1]:=0; G[2]:=0; G[3]:=0;*)
  if CheckBoxSendToHiddenAng.Checked then Form1.TabSheetAng.TabVisible:=false;
  Form1.SpeedButtonModeSimple.Down:=true;
  if not IsNullVector(AllotVecAng) then SetAllotation(AllotVecAng,clRed,clRed);
  if not IsNullVector(AllotVecAng2) then SetAllotation(AllotVecAng2,clRed,clRed);
  SetNullVector(AllotVecAng);
  SetNullVector(AllotVecAng2);
end;

procedure TAngleForm.FormActivate(Sender: TObject);
begin
  SpinEditVec1_X.Value:=FloatToInt(AllotVecAng2[1]);
  SpinEditVec1_Y.Value:=FloatToInt(AllotVecAng2[2]);
  SpinEditVec1_Z.Value:=FloatToInt(AllotVecAng2[3]);
  SpinEditVec2_X.Value:=FloatToInt(AllotVecAng[1]);
  SpinEditVec2_Y.Value:=FloatToInt(AllotVecAng[2]);
  SpinEditVec2_Z.Value:=FloatToInt(AllotVecAng[3]);
  ButtonAngCulc.Click;
end;

procedure TAngleForm.CheckBoxSendToHiddenAngClick(Sender: TObject);
begin
  (*if CheckBoxSendToHiddenAng.Checked then
  begin
    CheckBoxAngOnTop.Enabled:=false;
    FormStyle:=fsStayOnTop;
    TabSheetHiddenAng:=TTabSheet.Create(Form1.PageControl1);
    TabSheetHiddenAng.Parent:=Self;
    TabSheetHiddenAng.Caption:='����';
    TabSheetHiddenAng.Name:='TabSheetHiddenAng';
    TabSheetHiddenAng.PageControl:=Form1.PageControl1;
    Left:=Form1.Left+Form1.PageControl1.Left+8;
    Top:=Form1.Top+80;
    if OptionForm.Visible or (Form1.PageControl1.ActivePage=TabSheetHiddenOpt)
    then Form1.SpeedButtonModeOption.Down:=true
    else Form1.SpeedButtonModeSimple.Down:=true;
    {if Form1.PageControl1.ActivePage <> TabSheetHiddenAng then} AngleForm.Hide;
  end
  else
  begin
    CheckBoxAngOnTop.Enabled:=true;
    if CheckBoxAngOnTop.Checked then FormStyle:=fsStayOnTop
    else FormStyle:=fsNormal;
    TabSheetHiddenAng.free;
  end; *)
  Hide;
  if CheckBoxSendToHiddenAng.Checked then
  begin
    Left:=Form1.Left+Form1.PageControl1.Left+8;
    Top:=Form1.Top+76;
    FormStyle:=fsStayOnTop;
    CheckBoxAngOnTop.Enabled:=false;
    Form1.TabSheetAng.TabVisible:=true;
    Form1.PageControl1.ActivePage:=Form1.TabSheetAng;
  end  
  else
  begin
    Left:=GetMaxX-Width;
    Top:=PropertiesForm.Height-1;
    CheckBoxAngOnTop.Enabled:=true;
    if CheckBoxAngOnTop.Checked then FormStyle:=fsStayOnTop
    else FormStyle:=fsNormal;
    Form1.TabSheetAng.TabVisible:=false;
  end;
  Show;
end;

procedure TAngleForm.FormCreate(Sender: TObject);
begin
  Left:=GetMaxX-Width;
  Top:=PropertiesForm.Height-1;
end;

procedure TAngleForm.CheckBoxAngOnTopClick(Sender: TObject);
begin
  if CheckBoxAngOnTop.Checked then FormStyle:=fsStayOnTop else FormStyle:=fsNormal;
end;

end.
