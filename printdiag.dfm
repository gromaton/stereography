object FormPrint: TFormPrint
  Left = 193
  Top = 107
  BorderStyle = bsDialog
  Caption = 'Print'
  ClientHeight = 505
  ClientWidth = 492
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ImagePreview: TImage
    Left = 96
    Top = 8
    Width = 301
    Height = 434
  end
  object ButtonParam: TButton
    Left = 144
    Top = 464
    Width = 75
    Height = 25
    Caption = 'Settings'
    TabOrder = 0
    OnClick = ButtonParamClick
  end
  object ButtonPrint: TButton
    Left = 272
    Top = 464
    Width = 75
    Height = 25
    Caption = 'Print'
    TabOrder = 1
    OnClick = ButtonPrintClick
  end
end
