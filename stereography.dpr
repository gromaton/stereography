program stereography;

uses
  Forms,
  mainster in 'mainster.pas' {Form1},
  addfunc in 'addfunc.pas',
  solve2 in 'solve2.pas',
  resol in 'resol.pas' {FormResolution},
  options in 'options.pas' {PropertiesForm},
  Getangle in 'Getangle.pas' {AngleForm},
  printdiag in 'printdiag.pas' {FormPrint},
  mxcommon in '..\..\MXLib\mxcommon.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
