unit addfunc;

interface

uses Globtypes,Windows,SysUtils,Math,ComCtrls,Dialogs;

function GetMaxX(): Word; //����������� ��������������� ���������� ������
function GetMaxY(): Word; //����������� ������������� ���������� ������
function IntToFloat(x: Integer): Extended; //�������������� ������ ����� � ������������
function FloatToInt(x: Extended): Integer;
function Mdl(V: Vector): Double; //��������� ������ �������
function OrtVec(A: Vector): Vector; {���������� ������ ����������������� �������}
function VecxVec(A,B: Vector): Vector; {��������� ������������}
function Prot(x: Extended): Extended; //����������� ����� � ��������
function DistBtwnFlatness(a: Double; Vec: Vector): Double; //��������� �������������� ����������
function DegBragg(a,lambda: Double; Vec: Vector): Double; //����������� ���� ������
function AngleBtwnVectors(A,B: Vector): Extended; //���� ����� ���������
procedure ClearGraphicArea(hCanvas: HDC; x1,y1,x2,y2: Integer); //������� ����������� �������
function ExistTabInPageControl(TabName: string; PageControl: TPageControl): Boolean; //���������� ���� �� � PageControl'� �������� � ������ TabName
function CmprVectors(A,B: Vector): Boolean; //�������� �������� �� ���������
function GetEtalonDirs(MaxIndex: Byte; var N: integer): ManyVectors;
procedure ReadVectorFromString(Str: string; Prefix, Separator, Postfix: Char; var D: Vector);//������ ������� �� ������
function ReadVecsFromFile(FName: string; var Vecs: ManyVectors): Byte;
function AnalogVec(A: Vector): Vector;
function SameComponentVecs(A,B: Vector): Boolean;
function Nod2(a,b: Extended): Extended;
function Nod3(a,b,c: Extended): Extended;
function IsNullVector(V: Vector): boolean;

implementation

function GetMaxX(): Word;
begin
 Result:=GetDeviceCaps(GetDC(0),HORZRES);
end;

function GetMaxY(): Word;
begin
 Result:=GetDeviceCaps(GetDC(0),VERTRES);
end;

function IntToFloat(x: Integer): Extended;
begin
  Result:=StrToFloat(IntToStr(x));
end;

function FloatToInt(x: Extended): Integer;
begin
  Result:=StrToInt(FloatToStr(Trunc(x)));
end;

function Mdl(V: Vector): Double;
begin
 Result:=sqrt(sqr(V[1])+sqr(V[2])+sqr(V[3]));
end;

function OrtVec(A: Vector): Vector;
var i,S0,S0pos: Byte;
    S: Extended;
begin
 S:=0; S0:=0; S0pos:=0;
 for i:=1 to 3 do S:=S+abs(A[i]);
 for i:=1 to 3 do if A[i]=0 then begin S0:=S0+1;S0pos:=i;end;
 if S0=0 then begin Result[1]:=1; Result[2]:=1; Result[3]:=-(A[1]+A[2])/A[3]; end;
 if S0=1 then
 case S0pos of
   1: begin  Result[1]:=1; Result[2]:=1; Result[3]:=-A[2]/A[3]; end;
   2: begin  Result[1]:=1; Result[2]:=1; Result[3]:=-A[1]/A[3]; end;
   3: begin  Result[2]:=1; Result[3]:=1; Result[1]:=-A[2]/A[1]; end;
 end;
 if S0=2 then begin
        if A[1]<>0 then begin Result[1]:=0;Result[2]:=1;Result[3]:=0; end;
        if A[2]<>0 then begin Result[1]:=0;Result[2]:=0;Result[3]:=1; end;
        if A[3]<>0 then begin Result[1]:=1;Result[2]:=0;Result[3]:=0; end;
 end;
end;

function VecxVec(A,B: Vector): Vector;
begin
 Result[1]:=B[3]*A[2]-B[2]*A[3];
 Result[2]:=B[1]*A[3]-B[3]*A[1];
 Result[3]:=B[2]*A[1]-B[1]*A[2];
end;

function Prot(x: Extended): Extended;
begin
 Result:=0-x;
end;

function DistBtwnFlatness(a: Double; Vec: Vector): Double;
begin
    Result:=a/(sqrt(Vec[1]*Vec[1]+Vec[2]*Vec[2]+Vec[3]*Vec[3]));
end;

function DegBragg(a,lambda: Double; Vec: Vector): Double;
var d: Double;
begin
 d:=DistBtwnFlatness(a,Vec);
 if lambda > 2*d then
   result:=-1
 else
   result:=arcsin(abs(lambda/(2*d)))*180/PI;
end;

function AngleBtwnVectors(A,B: Vector): Extended;
var modA,modB,Fi,Arg: Extended;
begin
 modA:=sqrt(sqr(A[1])+sqr(A[2])+sqr(A[3]));
 modB:=sqrt(sqr(B[1])+sqr(B[2])+sqr(B[3]));
 Arg:=(A[1]*B[1]+A[2]*B[2]+A[3]*B[3])/(modA*modB);
 if Arg>1 then Arg:=1; if Arg<-1 then Arg:=-1;
 Fi:=ArcCos(Arg);
 Result:=Fi*180/Pi;
end;

procedure ClearGraphicArea(hCanvas: HDC; x1,y1,x2,y2: Integer);
begin
  PatBlt(hCanvas, x1, y1, x2, y2, WHITENESS);
end;

function ExistTabInPageControl(TabName: string; PageControl: TPageControl): Boolean;
var
  i: Word;
begin
  for i:=0 to PageControl.PageCount-1 do
    if PageControl.Pages[i].Name=TabName then
    begin
      Result:=true;
      Break;
    end
    else Result:=false;
end;

function CmprVectors(A,B: Vector): Boolean;
begin
  if (A[1]=B[1])and(A[2]=B[2])and(A[3]=B[3]) then Result:=true else Result:=false;
end;

function GetEtalonDirs(MaxIndex: Byte; var N: integer): ManyVectors;
var
  i,j,k: Word;
  Et: VeryManyVectors;
  N_et: integer;
  TheSame: Boolean;
begin
  N_et:=0;
  SetLength(Et,(MaxIndex+1)*(MaxIndex+1)*MaxIndex+1);
  for i:=0 to MaxIndex-1 do
    for j:=0 to MaxIndex do
      for k:=1 to MaxIndex do
      begin
        Inc(N_et);
        Et[N_et][1]:=i;
        Et[N_et][2]:=j;
        Et[N_et][3]:=k;
      end;
  N:=1;
  for i:=2 to N_et do
  begin
    TheSame:=false;
    Et[i]:=AnalogVec(Et[i]);
    for j:=1 to N do
      if SameComponentVecs(Et[i],Et[j]) then
      begin
        TheSame:=true;
        break;
      end;
    if not TheSame then
    begin
      Inc(N);
      Et[N]:=Et[i];
    end;
  end;
  //SetLength(Result,N+1);
  for i:=1 to N do
  Result[i]:=Et[i];
end;

procedure ReadVectorFromString(Str: string; Prefix, Separator, Postfix: Char; var D: Vector);
var
  i: Word;
  strVec: string;
begin
  i:=1;
  strVec:='';
  While Str[i]<>Prefix do Inc(i);
  Inc(i);
  While Str[i]<>Separator do
  begin
    strVec:=strVec+Str[i];
    Inc(i);
  end;
  D[1]:=StrToFloat(strVec);
  strVec:='';
  Inc(i);
  While Str[i]<>Separator do
  begin
    strVec:=strVec+Str[i];
    Inc(i);
  end;
  D[2]:=StrToFloat(strVec);
  strVec:='';
  Inc(i);
  While Str[i]<>Postfix do
  begin
    strVec:=strVec+Str[i];
    Inc(i);
  end;
  D[3]:=StrToFloat(strVec);
end;

function ReadVecsFromFile(FName: string; var Vecs: ManyVectors): Byte;
var
  MFile: TextFile;
  i: Word;
  StrWthVec: string;
begin
  i:=0;
  try
    AssignFile(MFile,FName);
    Reset(MFile);
    while not EoF(MFile) do
    begin
      Inc(i);
      ReadLn(MFile,StrWthVec);
      ReadVectorFromString(StrWthVec,'<',' ','>',Vecs[i]);
    end;
    Result:=i;
  finally
    CloseFile(MFile);
  end;
end;

function AnalogVec(A: Vector): Vector;
var
  d: Extended;
  i,s,Pos: Byte;
begin
  s:=0;
  for i:=1 to 3 do
    if A[i]=0 then
    begin
      s:=s+1;
      Pos:=i;
    end;
  case s of
  2:begin
      Result:=A;
      Result[Pos]:=1;
      exit;
    end;
  1:if Pos=1 then d:=Nod2(A[2],A[3])
    else if Pos=2 then d:=Nod2(A[1],A[3])
         else d:=Nod2(A[1],A[2]);
  else
    d:=Nod3(A[1],A[2],A[3]);
  end;
  Result[1]:=A[1]/d;
  Result[2]:=A[2]/d;
  Result[3]:=A[3]/d;
end;

function SameComponentVecs(A,B: Vector): Boolean;
var
  i,Pos: Byte;
  C,D: Vector;
begin
//????? ???? ?? ?????? ??????????
   Pos:=0;
   for i:=1 to 3 do
     if A[1]=B[i] then begin Pos:=i; break; end;
//
  case Pos of
  1:
    begin
      C[1]:=B[1]; C[2]:=B[2]; C[3]:=B[3];
      D[1]:=B[1]; D[2]:=B[3]; D[3]:=B[2];
    end;
  2:
    begin
      C[1]:=B[2]; C[2]:=B[1]; C[3]:=B[3];
      D[1]:=B[2]; D[2]:=B[3]; D[3]:=B[1];
    end;
  3:
    begin
      C[1]:=B[3]; C[2]:=B[1]; C[3]:=B[2];
      D[1]:=B[3]; D[2]:=B[2]; D[3]:=B[1];
    end;
  else
    begin
      Result:=false;
      Exit;
    end;
  end;
  if (CmprVectors(A,C))or(CmprVectors(A,D)) then Result:=true else Result:=false;
end;

function Nod2(a,b: Extended): Extended;
var
  i,nod: Extended;
  p: boolean;
begin
  p:=true;
  while p do begin
    if (a<b) then begin i:=a;a:=b;b:=i; end;
    if ({a mod b=0}Frac(a/b)=0) then begin nod:=b;p:=false; end;
    a:=a-b;
  end;
  Result:=nod;
end;

function Nod3(a,b,c: Extended): Extended;
var nod: Extended;
begin
  nod:=Nod2(a,b);
  Result:=Nod2(nod,c);
end;

function IsNullVector(V: Vector): boolean;
begin
  if (V[1]=0)and(V[2]=0)and(V[3]=0) then Result:=true else Result:=false;
end;


end.
