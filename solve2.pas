unit solve2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, jpeg, ComCtrls, CheckLst, Spin, Buttons, ExtDlgs, Math,
  Menus, globtypes, addfunc, printers;

function GetMainVector(x,y,z: Shortint): Vector; //������ ��� ��������
function MatrixOfConvert (A,B,C: Vector): Matrix; {������� ��������������}
function PointOnPlane (Pnt,n: Vector): Vector; //���������� ����� � ���������������� �.�.
function PointOnPlane2(Pnt, D: Vector): Vector; //���������� ����� ���� � ������������
function PointToNewSystem(Pnt: Vector; A: Matrix): Vector; //��������� ����� � ����� �.�.
function PointInPolar(Pnt,D: Vector): Vector; //�������������� � �������� �.�.
function TipDir(D: Vector): SomeDir; //���������� ���������� ����������� �� ����
function ptnsWithScale(Direction, P: Vector; spScale: Word): Direct;//������� ����� � ������ ��������
function TestOfTop(Dir,n: Vector): Boolean; {�������� �� �������������� ������� ���������}
function TestOfReflection(n: byte; //������� �������� �������
                          var Dir: Vector): Boolean; //���������� ���������
procedure DrawProjection(P,D: Vector; //������� ��������������� ������� ����� � ������� ���� ����������
                         pntCenter: TPoint; //����� �������
                         ShowIndex: Boolean; //������� �� ������� � �������
                         clrOut, clrIn: TColor; //���� ��������� � �������
                         OutPutImg: TBitMap); //��������� ������ ����������� D �� �������� � ���� P
procedure GetReflexSize(P,D: Vector; //������ �������� ������� ��������, �� ������ �������� ���������� ������
                        pntCenter: TPoint; //����� �������
                        var Cir: TRect //���������� �������, � ������� ����� ��������� �������
                        ); //����������� ������� �������� �� ��� ��������
function DistBtwnFlatness(a: Double; Vec: Vector): Double; //��������� �������������� ����������
function DegBragg(a,lambda: Double; Vec: Vector): Double; //����������� ���� ������
function GetCenterOfPoint(X,Y: SmallInt; Img: TBitmap): Direct;//����������� ������ ��������
function GetIndexOfPoint(x,y: SmallInt): Vector; //����������� ������� ��������
//procedure ReadVectorFromString(Str: string; Prefix, Separator, Postfix: Char; var D: Vector);//������ ������� �� ������
procedure ReadXYFromString(Str: string; Prefix, Separator, Postfix: Char; var vXY: Vector); overload;//������ ��������� �� ������
procedure ReadXYFromString(Str: string; Prefix, Separator, Postfix: Char; var vXY: TPoint); overload;
function VecToStr(D: Vector; Separator: string): string; //�������������� ������� � ������
procedure ReadReflexRFromString(Str: string; var ReflexR: Word);//������ ������� ��������
function FindStringWithVector(D: Vector): string; //����� ������ � ������ ���������
function DegreeBtwnVectors(A,B: Vector): Extended; //���� ����� ���������
Function PrinterCoordX(x:integer):integer;
function PrinterCoordY(Y:integer):integer;
function SetAllotation(D: Vector; clrOut,clrIn: TColor): Vector; //��������� ��������, return ����������
function PointOnPlane3(Pnt, D: Vector; var G: Extended): Vector;
procedure SetNullVector(var V: Vector);
function TipDir2(D: Vector; var N: Byte): SomeDir;

Var
  FirstDraw: Boolean;
  Rox: Word;
  MemVec: ManyVectors;

implementation

uses mainster;

function GetMainVector(x,y,z: Shortint): Vector;
begin
  Result[1]:=StrToFloat(IntToStr(x));
  Result[2]:=StrToFloat(IntToStr(y));
  Result[3]:=StrToFloat(IntToStr(z));
end;

function MatrixOfConvert(A,B,C: Vector): Matrix;
var
  modVec1: Extended;
  i: Byte;
begin
  modVec1:=sqrt(sqr(A[1])+sqr(A[2])+sqr(A[3]));
  for i:=1 to 3 do Result[1][i]:=A[i]/modVec1;
  modVec1:=sqrt(sqr(B[1])+sqr(B[2])+sqr(B[3]));
  for i:=1 to 3 do Result[2][i]:=B[i]/modVec1;
  modVec1:=sqrt(sqr(C[1])+sqr(C[2])+sqr(C[3]));
  for i:=1 to 3 do Result[3][i]:=C[i]/modVec1;
end;

function PointOnPlane(Pnt,n: Vector): Vector;
var
  Xc,Yc,Zc,A,B,C, R1, R2, R3,S,Psi, Fi,R: Extended;
begin
  Xc:=Pnt[1]/sqrt(sqr(Pnt[1])+sqr(Pnt[2])+sqr(Pnt[3]));
  Yc:=Pnt[2]/sqrt(sqr(Pnt[1])+sqr(Pnt[2])+sqr(Pnt[3]));
  Zc:=Pnt[3]/sqrt(sqr(Pnt[1])+sqr(Pnt[2])+sqr(Pnt[3]));
  A:=n[1]/sqrt(sqr(n[1])+sqr(n[2])+sqr(n[3]));
  B:=n[2]/sqrt(sqr(n[1])+sqr(n[2])+sqr(n[3]));
  C:=n[3]/sqrt(sqr(n[1])+sqr(n[2])+sqr(n[3]));
  if sqr(A)+sqr(B)+sqr(C)+A*Xc+B*Yc+C*Zc=0 then Exit;
  R1:=(Xc*(sqr(C)+sqr(B))-A*B*Yc-A*C*Zc)/(sqr(A)+sqr(B)+sqr(C)+A*Xc+B*Yc+C*Zc);
  R2:=(Yc*(sqr(A)+sqr(C))-A*B*Xc-B*C*Zc)/(sqr(A)+sqr(B)+sqr(C)+A*Xc+B*Yc+C*Zc);
  R3:=(Zc*(sqr(A)+sqr(B))-A*C*Xc-B*C*Yc)/(sqr(A)+sqr(B)+sqr(C)+A*Xc+B*Yc+C*Zc);
  Result[1]:=R1;
  Result[2]:=R2;
  Result[3]:=R3;
end;

function PointOnPlane2(Pnt, D: Vector): Vector;
var
  Deg{���� ����� Pnt � D},   A,B,C{������� �����},
  dx,dy,dz{�������},    X,Y,Z{���������� ��������},
  Tmp{���� � �� �� ������������}: Extended;
begin
  Deg:=AngleBtwnVectors(Pnt,D);
  if 2*Deg<=45 then
  begin
    A:=Pnt[1]/sqrt(sqr(Pnt[1])+sqr(Pnt[2])+sqr(Pnt[3]));
    B:=Pnt[2]/sqrt(sqr(Pnt[1])+sqr(Pnt[2])+sqr(Pnt[3]));
    C:=Pnt[3]/sqrt(sqr(Pnt[1])+sqr(Pnt[2])+sqr(Pnt[3]));
    dx:=D[1]/sqrt(sqr(D[1])+sqr(D[2])+sqr(D[3]));
    dy:=D[2]/sqrt(sqr(D[1])+sqr(D[2])+sqr(D[3]));
    dz:=D[3]/sqrt(sqr(D[1])+sqr(D[2])+sqr(D[3]));
    Tmp:={(sqr(A)+sqr(B)+sqr(C))}1/(A*dx+B*dy+C*dz);
    X:=Tmp*dx;
    Y:=Tmp*dy;
    Z:=Tmp*dz;
    Stopbit:=false;
  end
  else
  begin
    Stopbit:=true;
    X:=1; Y:=1; Z:=1;
  end;
  Result[1]:=X;
  Result[2]:=Y;
  Result[3]:=Z;
end;

function PointOnPlane3(Pnt, D: Vector; var G: Extended): Vector;
var
  Deg{���� ����� Pnt � D},   A,B,C{������� �����},
  dx,dy,dz{�������},    X,Y,Z{���������� ��������},
  Tmp{���� � �� �� ������������},S,l: Extended;

begin
  Deg:=AngleBtwnVectors(Pnt,D);
  if 2*Deg<=45 then
  begin
    Result:=PointOnPlane(D,Pnt);
    A:=Pnt[1]/sqrt(sqr(Pnt[1])+sqr(Pnt[2])+sqr(Pnt[3]));
    B:=Pnt[2]/sqrt(sqr(Pnt[1])+sqr(Pnt[2])+sqr(Pnt[3]));
    C:=Pnt[3]/sqrt(sqr(Pnt[1])+sqr(Pnt[2])+sqr(Pnt[3]));
    dx:=D[1]/sqrt(sqr(D[1])+sqr(D[2])+sqr(D[3]));
    dy:=D[2]/sqrt(sqr(D[1])+sqr(D[2])+sqr(D[3]));
    dz:=D[3]/sqrt(sqr(D[1])+sqr(D[2])+sqr(D[3]));
    S:={Mdl(Pnt)*}tan(2*Deg*Pi/180);
    l:=sqrt(sqr(Result[1])+sqr(Result[2])+sqr(Result[3]));
    if l=0 then G:=1 else G:=S/l;
    Stopbit:=false;
  end
  else
  begin
    Stopbit:=true;
    Result[1]:=1;
    Result[2]:=1;
    Result[3]:=1;
  end;
end;

function PointToNewSystem(Pnt: Vector; A: Matrix): Vector;
var
  i,j: Byte;
begin
 for i:=1 to 3 do
 begin
   Result[i]:=0;
   for j:=1 to 3 do Result[i]:=Result[i]+A[i][j]*Pnt[j];
 end;
end;

function PointInPolar(Pnt,D: Vector): Vector;
Var R1,R2,
    R{���������� �� ����� �����������}, S{���������� �� ��������},
    Fi{���� ����� D � P}, rad,ug ,Psi{���� ����� ������ � ���� X}: Extended;
begin
 Result[4]:=0;
 R2:=sqrt(sqr(Pnt[1])+sqr(Pnt[2]));
 if R2=0 then
 begin
   Result[4]:=-1;
   exit;
 end;
 if Pnt[2]>=0 then
  R1:=ArcCos(Pnt[1]/R2)*180/Pi
 else
  R1:=ArcCos(-Pnt[1]/R2)*180/Pi+180;
 if Form1.RadioGroupBuildType.ItemIndex=1 then
 begin
   Fi:=AngleBtwnVectors(P,D);
   if ((R1>45)and(R1<=135))or((R1>225)and(R1<=315)) then ug:=1/Tan(R1*Pi/180)
   else if ((R1>135)and(R1<=225))or((R1>315)and(R1<=360))or((R1>=0)and(R1<=45)) then ug:=Tan(R1*Pi/180);
   rad:=sqrt(sqr(ug)+1);
   if 2*Fi>=Arctan(rad)*180/Pi then
   begin
     Result[4]:=-1;
     exit;
   end;
   R2:=Tan(2*Fi*Pi/180);
 end; 
 Result[1]:=R1;
 Result[2]:=R2;
end;

function TipDir(D: Vector): SomeDir;
var i,j: Byte;
    T: SomeDir;
begin
  T[1]:=D;
  T[7]:=D; T[7][1]:=Prot(D[1]);
  T[13]:=D; T[13][2]:=Prot(D[2]);
  T[19]:=D; T[19][3]:=Prot(D[3]);
  T[25]:=T[7]; T[25][2]:=Prot(D[2]);
  T[31]:=T[13]; T[31][3]:=Prot(D[3]);
  T[37]:=T[19]; T[37][1]:=Prot(D[1]);
  T[43]:=T[37]; T[43][2]:=Prot(D[2]);
  j:=1;
  for i:=1 to 8 do begin
   T[j+1]:=T[j]; T[j+1][2]:=T[j][3]; T[j+1][3]:=T[j][2];
   T[j+2]:=T[j]; T[j+2][1]:=T[j][2]; T[j+2][2]:=T[j][1];
   T[j+3][1]:=T[j][2]; T[j+3][2]:=T[j][3]; T[j+3][3]:=T[j][1];
   T[j+4][1]:=T[j][3]; T[j+4][2]:=T[j][1]; T[j+4][3]:=T[j][2];
   T[j+5]:=T[j]; T[j+5][1]:=T[j][3]; T[j+5][3]:=T[j][1];
   j:=j+6;
  end;
  Result:=T;
end;

function TipDir2(D: Vector; var N: Byte): SomeDir;
var i,j,Num,Tst: Byte;
    T: SomeDir;
begin
  T[1]:=D;
  T[7]:=D; T[7][1]:=Prot(D[1]);
  T[13]:=D; T[13][2]:=Prot(D[2]);
  T[19]:=D; T[19][3]:=Prot(D[3]);
  T[25]:=T[7]; T[25][2]:=Prot(D[2]);
  T[31]:=T[13]; T[31][3]:=Prot(D[3]);
  T[37]:=T[19]; T[37][1]:=Prot(D[1]);
  T[43]:=T[37]; T[43][2]:=Prot(D[2]);
  j:=1;
  for i:=1 to 8 do begin
   T[j+1]:=T[j]; T[j+1][2]:=T[j][3]; T[j+1][3]:=T[j][2];
   T[j+2]:=T[j]; T[j+2][1]:=T[j][2]; T[j+2][2]:=T[j][1];
   T[j+3][1]:=T[j][2]; T[j+3][2]:=T[j][3]; T[j+3][3]:=T[j][1];
   T[j+4][1]:=T[j][3]; T[j+4][2]:=T[j][1]; T[j+4][3]:=T[j][2];
   T[j+5]:=T[j]; T[j+5][1]:=T[j][3]; T[j+5][3]:=T[j][1];
   j:=j+6;
  end;
  T[0][1]:=0; T[0][2]:=0; T[0][3]:=0;
  Num:=1;
  for i:=1 to 48 do
    begin
      Tst:=0;
      for j:=1 to Num do
      if CmprVectors(T[i],T[j-1]) then Inc(Tst);
      if Tst = 0 then
      begin
        T[Num]:=T[i];
        Inc(Num);
      end;
    end;
  N:=Num-1;
  Result:=T;
end;

function ptnsWithScale(Direction, P: Vector; spScale: Word): Direct;
var A,V,N,M: Vector;
    T: Matrix;
    Con: SmallInt;
    PinP: Vector;
    G: Extended;
begin
 Con:=Form1.SpinEditRotate.Value;
 N:=OrtVec(P);
 M:=VecxVec(P,N);
 T:=MatrixOfConvert(N,M,P);
 V:=PointOnPlane(Direction,P);
 A:=PointToNewSystem(V,T);
 PinP:=PointInPolar(A,Direction);
 if PinP[4]=-1 then
 begin
   PinP[1]:=0;
   PinP[2]:=0;
 end;
 PinP[1]:=PinP[1]-Con;
 A[2]:=PinP[2]*sin(PinP[1]*Pi/180);
 A[1]:=PinP[2]*cos(PinP[1]*Pi/180);
 Result[1]:=Round(A[1]*spScale);
 Result[2]:=Round(A[2]*spScale);
 Result[3]:=Round(A[3]*spScale);
end;

function TestOfTop(Dir, n: Vector): Boolean;
var modn,modDir,scal, Fi: Extended;
begin
  Fi:=AngleBtwnVectors(n,Dir);
  if  Fi>90 then Result:=false else Result:=true;
end;

function TestOfReflection(n: byte; //������� �������� �������
                          var Dir: Vector): Boolean;
var sum: Word;
    chet, Catch: Boolean;
    i: Byte;
    J: Vector;
begin
 if CmprVectors(P,Dir) then //������� ����� �� ����������� �� ���������
 begin
   Result:=true;
   exit;
 end;
 Result:=true; Catch:=false;
 if Form1.CheckBoxFilter.Checked=true then
 begin
   for i:=1 to n do
   begin
     J[1]:=Dir[1]*i; J[2]:=Dir[2]*i; J[3]:=Dir[3]*i;
     if (abs(J[1])>n)or(abs(J[2])>n)or(abs(J[3])>n) then exit;
     sum:=Round(abs(J[1])+abs(J[2])+abs(J[3]));
     if (Round(J[1]) mod 2 =0)and(Round(J[2]) mod 2 =0)and(Round(J[3]) mod 2 =0) then chet:=true else chet:=false;
     if ((Round(J[1]) mod 2 <>0)and(Round(J[2]) mod 2 <>0)and(Round(J[3]) mod 2 <>0))or((sum mod 4 =0)and(chet=true))
       then Result:=true
     else Result:=false;
     if Result=true then
     begin
       Dir:=J;
       Exit;
     end;
   end;
 end;
end;

procedure DrawProjection(P,D: Vector; //������� ��������������� ������� ����� � ������� ���� ����������
                         pntCenter: TPoint; //����� �������
                         ShowIndex: Boolean; //������� �� ������� � �������
                         clrOut, clrIn: TColor; //���� ��������� � �������
                         OutPutImg: TBitMap);
var  Point: Direct;
     Cir: TRect;
     NameDir, strDir, strXY, strReflexR: String;
     OldBkMode,TxtLPos : integer;
     ReflexR: Byte;
begin
  StopBit:=false;
  OutPutImg.Canvas.Pen.Color:=clrOut;
  OutPutImg.Canvas.Brush.Color:=clrIn;
  Point:=ptnsWithScale(D,P,spScale-Round(spScale/20));
  GetReflexSize(P,D,pntCenter,Cir);
  Cir.Left:=Cir.Left+Point[2];
  Cir.Top:=Cir.Top+Point[1];
  Cir.Right:=Cir.Right+Point[2];
  Cir.Bottom:=Cir.Bottom+Point[1];
  ReflexR:=(Cir.Right-Cir.Left) div 2;
  strDir:='Direction: <'+VecToStr(D,' ')+'>; ';
  strXY:='Coordinates: ('+IntToStr(1+(Cir.Left+Cir.Right) div 2)+','+IntToStr(1+(Cir.Bottom+Cir.Top) div 2)+'); ';
  strReflexR:='Radius: R = '+IntToStr((Cir.Right-Cir.Left) div 2)+'; ';
  Writeln(TempFile,strDir+strXY+strReflexR);
  OutPutImg.Canvas.Ellipse(Cir);  //��������� ������ �����
  OutPutImg.Canvas.Brush.Color:=clWhite;
  if ShowIndex=true then
  begin
     NameDir:=FloatToStr(D[1])+FloatToStr(D[2])+FloatToStr(D[3]);
     TxtLPos:=(Cir.Left+Cir.Right)div 2 - OutPutImg.Canvas.TextWidth(NameDir) div 2;
     OldBkMode := SetBkMode(OutPutImg.Canvas.Handle,TRANSPARENT);
     OutPutImg.Canvas.TextOut(TxtLPos,-{7}Round(spScale/90)+Cir.Top - OutPutImg.Canvas.Font.Size,NameDir);
     SetBkMode(OutPutImg.Canvas.Handle,OldBkMode);
  end;
end;

procedure GetReflexSize(P,D: Vector; //������ �������� ������� ��������, �� ������ �������� ���������� ������
                        pntCenter: TPoint; //����� �������
                        var Cir: TRect //���������� �������, � ������� ����� ��������� �������
                        );
var
  dR: Byte; //������ ��������
begin
  if (Round(abs(D[1])+abs(D[2])+abs(D[3]))=1)or
     ((abs(D[1])=1)and(abs(D[2])=1)and(abs(D[3])=1)) then dR:=Round(8*(spScale/200))
  else if (abs(D[1])+abs(D[2])+abs(D[3])=2) then dR:=Round(5*(spScale/200))
       else dR:=Round(2*(spScale/200));
  if CmprVectors(D,P) then dR:=Round(7*(spScale/200));
  Cir.Left:=pntCenter.X-dR;
  Cir.Top:=pntCenter.Y-dR;
  Cir.Right:=pntCenter.X+dR;
  Cir.Bottom:=pntCenter.Y+dR;
end;

function DistBtwnFlatness(a: Double; Vec: Vector): Double;
begin
  Result:=a/(sqrt(Vec[1]*Vec[1]+Vec[2]*Vec[2]+Vec[3]*Vec[3]));
end;

function DegBragg(a,lambda: Double; Vec: Vector): Double;
var d: Double;
begin
 d:=DistBtwnFlatness(a,Vec);
 if lambda > 2*d then
   result:=-1
 else
   result:=arcsin(abs(lambda/(2*d)))*180/PI;
end;

function DegreeBtwnVectors(A,B: Vector): Extended;
var modA,modB,Fi,Arg: Extended;
begin
 modA:=sqrt(sqr(A[1])+sqr(A[2])+sqr(A[3]));
 modB:=sqrt(sqr(B[1])+sqr(B[2])+sqr(B[3]));
 Arg:=(A[1]*B[1]+A[2]*B[2]+A[3]*B[3])/(modA*modB);
 if Arg>1 then Arg:=1; if Arg<-1 then Arg:=-1;
 Fi:=ArcCos(Arg);
 Result:=Fi*180/Pi;
end;

//���������� ����������� ��� ��������� ������� ������ ���� �� ����� �������� � ����������� ������� ����� ��������
function GetCenterOfPoint(X,Y: SmallInt; Img: TBitmap): Direct;
Var ImgDC: HDC;
    Rvalue, Gvalue, Bvalue, RvalueClk, GvalueClk, BvalueClk, dk: Byte;
    GotAPoint,GotColor: Boolean;
    k1,k2: SmallInt;
    PixelColor: Longword;
begin
 ImgDC:=Form1.ImageMain.Canvas.Handle;
 PixelColor:=GetPixel(ImgDC,X,Y);
 RvalueClk:=GetRValue(GetPixel(ImgDC,X,Y));
 GvalueClk:=GetGValue(GetPixel(ImgDC,X,Y));
 BvalueClk:=GetBValue(GetPixel(ImgDC,X,Y));
// ShowMessage(IntToStr(GetPixel(ImgDC,X,Y)));
 if (PixelColor=16777215)or(PixelColor=0)or(PixelColor=132) then GotAPoint:=false
  else GotAPoint:=true;
 if GotAPoint=true then
  begin
   dk:=0;
   repeat     //����������� ������ �� ��� �
    dk:=dk+1;
    Gvalue:=GetGValue(GetPixel(ImgDC,X-dk,Y));
    Rvalue:=GetRValue(GetPixel(ImgDC,X-dk,Y));
    Bvalue:=GetBValue(GetPixel(ImgDC,X-dk,Y));
    if (Rvalue=RvalueClk)and(Gvalue=GvalueClk)and(Bvalue=BvalueClk) then GotColor:=true else GotColor:=false;
   until GotColor=false;
   k1:=X-dk; dk:=0;
   repeat
    dk:=dk+1;
    Gvalue:=GetGValue(GetPixel(ImgDC,X+dk,Y));
    Rvalue:=GetRValue(GetPixel(ImgDC,X+dk,Y));
    Bvalue:=GetBValue(GetPixel(ImgDC,X+dk,Y));
    if (Rvalue=RvalueClk)and(Gvalue=GvalueClk)and(Bvalue=BvalueClk) then GotColor:=true else GotColor:=false;
   until GotColor=false;
   k2:=X+dk; dk:=0;
   Result[1]:=(k1+k2) div 2;

   repeat // ����������� ������ �� ��� Y
    dk:=dk+1;
    Gvalue:=GetGValue(GetPixel(ImgDC,X,Y-dk));
    Rvalue:=GetRValue(GetPixel(ImgDC,X,Y-dk));
    Bvalue:=GetBValue(GetPixel(ImgDC,X,Y-dk));
    if (Rvalue=RvalueClk)and(Gvalue=GvalueClk)and(Bvalue=BvalueClk) then GotColor:=true else GotColor:=false;
   until GotColor=false;
   k1:=Y-dk; dk:=0;
   repeat
    dk:=dk+1;
    Gvalue:=GetGValue(GetPixel(ImgDC,X,Y+dk));
    Rvalue:=GetRValue(GetPixel(ImgDC,X,Y+dk));
    Bvalue:=GetBValue(GetPixel(ImgDC,X,Y+dk));
    if (Rvalue=RvalueClk)and(Gvalue=GvalueClk)and(Bvalue=BvalueClk) then GotColor:=true else GotColor:=false;
   until GotColor=false;
   k2:=Y+dk;
   Result[2]:=(k1+k2) div 2;
  end
  else begin Result[1]:=0; Result[2]:=0; end;
end;

function GetIndexOfPoint(x,y: SmallInt): Vector;
var Stroka,Num: String;
    Fx,Fy: SmallInt;
    i: Byte;
    Vect: Vector;
    Founded: Boolean;
begin
 Reset(TempFile);
 Vect[1]:=0; Vect[2]:=0; Vect[3]:=0;
 if (x=0)and(y=0) then
  begin
   Founded:=true;
   Result[1]:=0; Result[2]:=0; Result[3]:=0;
  end
 else Founded:=false;
 While (Founded<>true) do
  begin
   Stroka:=''; Num:='';
   ReadLn(TempFile,Stroka);
   ReadXYFromString(Stroka,'(',',',')',Vect);
   if (IntToFloat(x)=Vect[1])and(IntToFloat(y)=Vect[2]) then
    begin
     ReadVectorFromString(Stroka,'<',' ','>',Result);
     Founded:=true;
     exit;
    end;
   if Eof(TempFile) then
    begin
     Result[1]:=0; Result[2]:=0; Result[3]:=0;
     Founded:=true;
    end;
  end;
 CloseFile(TempFile);
 //ShowMessage(IntToStr(Rox));}
end;
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
procedure ReadXYFromString(Str: string; Prefix, Separator, Postfix: Char; var vXY: Vector);
var
  i: Word;
  strVec: string;
begin
  i:=1;
  strVec:='';
  While Str[i]<>Prefix do Inc(i);
  Inc(i);
  While Str[i]<>Separator do
  begin
    strVec:=strVec+Str[i];
    Inc(i);
  end;
  vXY[1]:=StrToFloat(strVec);
  strVec:='';
  Inc(i);
  While Str[i]<>Postfix do
  begin
    strVec:=strVec+Str[i];
    Inc(i);
  end;
  vXY[2]:=StrToFloat(strVec);
end;

procedure ReadXYFromString(Str: string; Prefix, Separator, Postfix: Char; var vXY: TPoint);
var
  i: Word;
  strVec: string;
begin
  i:=1;
  strVec:='';
  While Str[i]<>Prefix do Inc(i);
  Inc(i);
  While Str[i]<>Separator do
  begin
    strVec:=strVec+Str[i];
    Inc(i);
  end;
  vXY.X:=StrToInt(strVec);
  strVec:='';
  Inc(i);
  While Str[i]<>Postfix do
  begin
    strVec:=strVec+Str[i];
    Inc(i);
  end;
  vXY.Y:=StrToInt(strVec);
end;

procedure ReadReflexRFromString(Str: string; var ReflexR: Word);
var
  i: Word;
  strR: string;
begin
  i:=1;
  strR:='';
  While Str[i]<>'=' do Inc(i);
  i:=i+2;
  While Str[i]<>';' do
  begin
    strR:=strR+Str[i];
    Inc(i);
  end;
  ReflexR:=StrToInt(strR);
end;

function FindStringWithVector(D: Vector): string;
var
  CurStr: string;
  V: Vector;
begin
  if (D[1]=0)and(D[2]=0)and(D[3]=0) then
  begin
    ShowMessage('Error: Trying to find null Vector in TempFile');
    Halt;
  end;
  Reset(TempFile);
  CurStr:='';
  V[1]:=MinSingle; V[2]:=MinSingle; V[3]:=MinSingle;
  While VecToStr(D,' ')<>VecToStr(V,' ') do
  begin
    ReadLn(TempFile,CurStr);
    ReadVectorFromString(CurStr,'<',' ','>',V);
    if (EoF(TempFile))and(VecToStr(D,' ')<>VecToStr(V,' ')) then
    begin
      Result:='';
      CloseFile(TempFile);
      exit;                        
    end;
  end;
  CloseFile(TempFile);
  Result:=CurStr;
end;
                                       

function VecToStr(D: Vector; Separator: string): string;
begin
  Result:=FloatToStr(D[1])+Separator+FloatToStr(D[2])+Separator+FloatToStr(D[3]);
end;

function PrinterCoordX(x:integer):integer; { ��������� ���������� �� �� � ������� }
begin
  PixelsX:=GetDeviceCaps(printer.Handle,LogPixelsX);
  Result:=round((PixelsX/25.4)*x);
end;

function PrinterCoordY(Y:integer):integer; { ��������� ���������� �� �� � ������� }
begin
  PixelsY:=GetDeviceCaps(printer.Handle,LogPixelsY);
  Result:=round((PixelsY/25.4)*Y);
end;

function SetAllotation(D: Vector; clrOut,clrIn: TColor): Vector;
var
  oldBrushColor,oldPenColor: TColor;
  strintmp: string;
  Center: TPoint;
  Radius: Word;
begin
  oldBrushColor:=Form1.ImageMain.Canvas.Brush.Color;
  oldPenColor:=Form1.ImageMain.Canvas.Pen.Color;
  strintmp:=FindStringWithVector(D);
  if strintmp='' then
  begin
    D:=P;
    AllotVecOpt:=D;
    strintmp:=FindStringWithVector(D);
  end;
  ReadXYFromString(strintmp,'(',',',')',Center);
  Inc(Center.X);
  Inc(Center.Y);
  ReadReflexRFromString(strintmp,Radius);
  if (clrIn = clRed)and(clrOut = clRed)and(CmprVectors(D,P)) then
  begin
    clrIn:=clDkGray;
    clrOut:=clDkGray;
  end;
  with Form1 do
  begin
    ImageMain.Canvas.Brush.Color:=clrIn;
    ImageMain.Canvas.Pen.Color:=clrOut;
    ImageMain.Canvas.Ellipse(Center.X-Radius,Center.Y-Radius,Center.X+Radius,Center.Y+Radius);
    ImageMain.Canvas.FloodFill(Center.X,Center.Y,clrOut,fsBorder);
    ImageMain.Canvas.Brush.Color:=oldBrushColor;
    ImageMain.Canvas.Pen.Color:=oldPenColor;
  end;
  Result:=D;
  Result[4]:=Radius;
end;

procedure SetNullVector(var V: Vector);
begin
  V[1]:=0; V[2]:=0; V[3]:=0;
end;


end.
