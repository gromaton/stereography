unit mainster;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Jpeg,
  Dialogs, Menus, ComCtrls, ExtCtrls, StdCtrls, Spin, CheckLst, Addfunc, Solve2, Globtypes,
  Resol, Options, printers, Buttons, Getangle, Math, Filesmyx, IniFiles, PrintDiag;

type
  TForm1 = class(TForm)
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Exit1: TMenuItem;
    N1: TMenuItem;
    Print1: TMenuItem;
    N2: TMenuItem;
    MainMenuSaveImageAs: TMenuItem;
    Open1: TMenuItem;
    Edit1: TMenuItem;
    Window1: TMenuItem;
    PageControl1: TPageControl;
    ImageMain: TImage;
    SaveAssemblageAs1: TMenuItem;
    TabSheetMain: TTabSheet;
    TabSheetAssemblage: TTabSheet;
    N3: TMenuItem;
    GroupBoxMainAxis: TGroupBox;
    SpinMainAxisX: TSpinEdit;
    SpinMainAxisY: TSpinEdit;
    SpinMainAxisZ: TSpinEdit;
    ComboBoxMainAxis: TComboBox;
    ButtonRefresh: TButton;
    GroupBoxRotate: TGroupBox;
    StaticTextRotate: TStaticText;
    SpinEditRotate: TSpinEdit;
    GroupBoxImgOpt: TGroupBox;
    CheckBoxShowIndex: TCheckBox;
    CheckBoxFilter: TCheckBox;
    CheckListBoxAssemblage: TCheckListBox;
    StaticTextCurrentAssemblage: TStaticText;
    RadioGroupBuildType: TRadioGroup;
    StaticTextDefaultAssemblage: TStaticText;
    SaveSteprDialog: TSaveDialog;
    PopupMenuOptions: TPopupMenu;
    PopupItemOptions: TMenuItem;
    MainMenuRefresh: TMenuItem;
    PopupMakeMain: TMenuItem;
    PanelInstruments: TPanel;
    SpeedButtonModeSimple: TSpeedButton;
    SpeedButtonModeOption: TSpeedButton;
    SpeedButtonModeAngle: TSpeedButton;
    PopupAngle: TMenuItem;
    N4: TMenuItem;
    CheckBoxUpToIndex: TCheckBox;
    SpinEditUpToIndex: TSpinEdit;
    SaveAssemblageDialog: TSaveDialog;
    OpenAssemblageDialog: TOpenDialog;
    Label1: TLabel;
    Label2: TLabel;
    TabSheetOpt: TTabSheet;
    TabSheetAng: TTabSheet;
    PrinterSetupDialog1: TPrinterSetupDialog;
    CheckBoxXYAxis: TCheckBox;

    procedure WMGetMinMaxInfo(var Message : TMessage); message WM_GETMINMAXINFO;
    procedure WMExitSizeMove(var Message: TMessage); message WM_EXITSIZEMOVE;
    procedure WMEnterSizeMove(var Message: TMessage); message WM_ENTERSIZEMOVE;
    procedure WMSysCommand(var Message: TMessage); message WM_SYSCOMMAND;
    procedure WMNCHitTest(var M: TWMNCHitTest); message wm_NCHitTest;
    procedure WMLButtonDblClk(var Message: Tmessage); message WM_NCLBUTTONDBLCLK;

    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ComboBoxMainAxisChange(Sender: TObject);
    procedure SpinMainAxisXChange(Sender: TObject);
    procedure SetNewAssemblage(ColFName: string; OnlyShown: Boolean);
    procedure SetMaxIndex(MaxInd: Byte);
    procedure ButtonRefreshClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure Exit1Click(Sender: TObject);
    procedure SpinEditRotateExit(Sender: TObject);
    procedure CheckBoxShowIndexClick(Sender: TObject);
    procedure MainMenuSaveImageAsClick(Sender: TObject);
    procedure PopupItemOptionsClick(Sender: TObject);
    procedure ImageMainMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PopupMakeMainClick(Sender: TObject);
    procedure SpinMainAxisXClick(Sender: TObject);
    procedure SpinMainAxisYClick(Sender: TObject);
    procedure SpinMainAxisZClick(Sender: TObject);
    procedure SpinEditRotateClick(Sender: TObject); //��������� ������ ������ �� �����
    procedure GetPrinterInfo();
    procedure SpeedButtonModeOptionClick(Sender: TObject);
    procedure PopupAngleClick(Sender: TObject);
    procedure SpeedButtonModeAngleClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure FormEndDock(Sender, Target: TObject; X, Y: Integer);
    procedure N4Click(Sender: TObject);
    procedure CheckBoxUpToIndexClick(Sender: TObject);
    procedure SaveAssemblageAs1Click(Sender: TObject);
    procedure Open1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure N3Click(Sender: TObject);
    procedure MainMenuRefreshClick(Sender: TObject);
    procedure TabSheetOptShow(Sender: TObject);
    procedure TabSheetAngShow(Sender: TObject);
    procedure MainAxisOnChange(Available: boolean);
    procedure SpeedButtonModeSimpleClick(Sender: TObject);
    procedure Print1Click(Sender: TObject);
    procedure DrawXYAxises();
    procedure CheckBoxXYAxisClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  function BuildRealLaue(LtoFilm: Word): TPoint;
procedure AssignSteprToImageMain(); //������� �� Stepr'� �� ����� ImageMain'a
procedure DrawRectAround(); //������ ����� ������ �����������
procedure DrawGraphics(BuildType: Byte; //��� ��������? �������������� ��� ����������
                       pntCenter: TPoint; //�������� ���������� ������ �������
                       var Img: TBitmap //���� �������� �����������
                       ); //������ ��� ���� �� ����������� �������
procedure AdditionalDraw(D_old, D_new: Vector); //��������� ���������
procedure LoadLgSettings;
procedure SaveLgSettings;

var
  Form1: TForm1;
  CurInCaption {������ �� ���������},TextOutScaled {���������� ���������������}: Boolean;
  TabSheetHiddenOpt, TabSheetHiddenAng: TTabSheet;

implementation

{$R *.dfm}

// ����������� ������� ��������� � ����������� �������� ����
procedure TForm1.WMGetMinMaxInfo(var Message : TMessage);
type
  PTMinMaxInfo = ^TMinMaxInfo;
begin
  with PTMinMaxInfo(Message.LParam)^.ptMinTrackSize
   do begin
     x := 540;
     y := 400;
   end;
  inherited;
end;

procedure TForm1.WMExitSizeMove(var Message : TMessage);
begin
  AssignSteprToImageMain;
  TextOutScaled:=false;
end;

procedure TForm1.WMEnterSizeMove(var Message: TMessage);
begin
  ClearGraphicArea(Form1.ImageMain.Canvas.Handle,0,0,Form1.ImageMain.Width,Form1.ImageMain.Height);
  TextOutScaled:=true;
end;


procedure TForm1.WMSysCommand(var Message: TMessage);
begin
  inherited;
  if (Message.WParam = SC_MAXIMIZE)or(Message.WParam = SC_RESTORE) then AssignSteprToImageMain;
end;

procedure TForm1.WMNCHitTest(var M: TWMNCHitTest);
begin
  inherited;
  if M.Result = htCaption then
  begin
    CurInCaption:=true;
  end;
end;

procedure TForm1.WMLButtonDblClk(var Message: Tmessage);
begin
  inherited;
  if CurInCaption=true then
  begin
    AssignSteprToImageMain;
    CurInCaption:=false;
  end;
end;
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

//������� �������� ����
procedure TForm1.Open1Click(Sender: TObject);
begin
  if OpenAssemblageDialog.Execute then
   SetNewAssemblage(OpenAssemblageDialog.FileName,false);
end;

procedure TForm1.SaveAssemblageAs1Click(Sender: TObject);
var
  i: integer;
  ColFile: TextFile;
  FName: string;
begin
  SaveAssemblageDialog.DefaultExt:='col';
  if SaveAssemblageDialog.Execute then
  begin
    FName:=SaveAssemblageDialog.FileName;
//�������� ������������� ������ �����,���� ���,�� ��������
    if FileExists(FName) then
      if MessageDlg('File '+FName+' is already exists. Rewrite?',
        mtConfirmation, [mbYes, mbNo], 0) <> mrYes
      then SaveAssemblageAs1Click(Self);
//
//������ ������ � ���� FName
    AssignFile(ColFile,FName);
    Rewrite(ColFile);
    for i:=0 to CheckListBoxAssemblage.Items.Count-1 do
      if CheckListBoxAssemblage.Checked[i] then
        WriteLn(ColFile,CheckListBoxAssemblage.Items.Strings[i]);
    CloseFile(ColFile);
  end;
//
  StaticTextCurrentAssemblage.Caption:=GetFileNameOnly(FName);
end;

procedure TForm1.MainMenuSaveImageAsClick(Sender: TObject);
var
  pntCenter: TPoint;
  FName: TFileName; //��� ������������ �����
  SteprJPG: TJPEGImage;
  CanSave: Boolean;
begin
  SaveSteprDialog.DefaultExt:='jpg';
  FormResolution.ShowModal;
//������� ���������� ��� ����������� ����
  if RadioGroupBuildType.ItemIndex=1 then SaveSteprDialog.Title:='Save Lauegram as ...'
  else SaveSteprDialog.Title:='Save stereographic projection as ...';
//
  if FormResolution.ModalResult<>mrOk then exit;
  if SaveSteprDialog.Execute then
  begin
    FName:=SaveSteprDialog.FileName;
    //�������� ������������� ������ �����,���� ���,�� ��������
    if FileExists(FName)and
       (MessageDlg('File '+FName+' is already exists. Rewrite?',
            mtConfirmation, [mbYes, mbNo], 0) <> mrYes)
      then
      begin
        MainMenuSaveImageAsClick(Self);
        exit;
      end;
    //
    //���������� �������� � ��������� ����������� � ������ ����������
    if not FormResolution.CheckBoxCurSize.Checked then
    begin
      if FormResolution.ComboBoxDpi.Enabled then
        Stepr.Width:=Trunc(StrToFloat(FormResolution.ComboBoxSquareSize.Text)
                    *StrToFloat(FormResolution.ComboBoxDpi.Text)/25.4)
      else
        Stepr.Width:=StrToInt(FormResolution.ComboBoxSquareSize.Text);
      Stepr.Height:=Stepr.Width;
      if Stepr.Width<Stepr.Height then spScale:=Stepr.Width div 2
      else spScale:=Stepr.Height div 2;
      pntCenter.X:=Stepr.Width div 2;
      pntCenter.Y:=Stepr.Height div 2;
      DrawGraphics(RadioGroupBuildType.ItemIndex,pntCenter,Stepr);
    end;
    //���������� � ������ �������
    if SaveSteprDialog.FilterIndex=1 then
    begin
      SteprJPG:=TJPEGImage.Create;
      SteprJPG.Assign(Stepr);
      SteprJPG.SaveToFile(FName);
      SteprJPG.Free;
    end
    else
      Stepr.SaveToFile(FName);
  end;
  AssignSteprToImageMain;
end;

procedure TForm1.Exit1Click(Sender: TObject);
begin
  Close();
end;

procedure TForm1.N3Click(Sender: TObject);
begin
  LgSett.WriteString('Common','Default assemblage',StaticTextCurrentAssemblage.Caption);
  StaticTextDefaultAssemblage.Caption:=StaticTextCurrentAssemblage.Caption;
end;

procedure TForm1.N4Click(Sender: TObject);
var
  strMaxInt: string;
begin
  strMaxInt:=InputBox('Max index','Input new value',IntToStr(MaxIndex));
  if strMaxInt<>'' then MaxIndex:=StrToInt(strMaxInt);
  SetMaxIndex(MaxIndex);
end;

procedure TForm1.MainMenuRefreshClick(Sender: TObject);
begin
  if SpinMainAxisX.Text='' then SpinMainAxisX.Undo
    else if SpinMainAxisY.Text='' then SpinMainAxisY.Undo
         else if SpinMainAxisZ.Text='' then SpinMainAxisZ.Undo;
  AssignSteprToImageMain();
end;
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

procedure TForm1.FormResize(Sender: TObject);
begin
// PageControl1
  PageControl1.Height:=ClientHeight;
// ImageMain
  ImageMain.Width:=ClientWidth-ImageMain.Left-1 ;
  ImageMain.Height:=ClientHeight;
// StaticTextDefaultAssemblage
  StaticTextDefaultAssemblage.Top:=TabSheetAssemblage.Height-StaticTextDefaultAssemblage.Height;
  Label2.Top:=TabSheetAssemblage.Height-StaticTextDefaultAssemblage.Height;
// CheckListBoxAssemblage
  CheckListBoxAssemblage.Height:=TabSheetAssemblage.Height-CheckListBoxAssemblage.Top-StaticTextDefaultAssemblage.Height-30;
//UpToIndex components
  CheckBoxUpToIndex.Top:=CheckListBoxAssemblage.Top+CheckListBoxAssemblage.Height+7;
  SpinEditUpToIndex.Top:=CheckBoxUpToIndex.Top-3;
// PanelInstruments
  PanelInstruments.Height:=ClientHeight;
// ����� ������ "��������������� ..."
  if TextOutScaled=true then
  begin
    DrawRectAround;
    ImageMain.Canvas.TextOut(ImageMain.Width div 2 - 80, ImageMain.Height div 2 - ImageMain.Canvas.Font.Size div 2, 'Scaling...');
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
//������� �����
  Left:=10;
  Top:=20;
//
  LgSett:=TIniFile.Create(GetCurrentDir+'\lgraph.ini');
  OldJ[1]:=0; OldJ[2]:=0;
  AllotVecOpt[1]:=0; AllotVecOpt[2]:=0; AllotVecOpt[3]:=0;
  AllotVecAng[1]:=0; AllotVecAng[2]:=0; AllotVecAng[3]:=0;
  AllotVecAng2[1]:=0; AllotVecAng2[2]:=0; AllotVecAng2[3]:=0;
  RepaintVec[1]:=0; RepaintVec[2]:=0; RepaintVec[3]:=0;
//��������� ������ �� ���������
  MaxIndex:=7;
  SetNewAssemblage(LgSett.ReadString('Common','Default assemblage','nil'),false);
//
  Stepr:=TBitmap.Create;
  Stepr.Width:=GetMaxX;
  Stepr.Height:=GetMaxY;
// ��������� ������� ��� ������� �������
  Stepr.Canvas.Font.Name:='Times New Roman';
  Stepr.Canvas.Font.size:=7;
//
  ImageMain.Picture.Assign(Stepr);
  ImageMain.Width:=ClientWidth-ImageMain.Left-1 ;
  ImageMain.Height:=ClientHeight;
  PropertiesForm:=TPropertiesForm.Create(Self);
  AngleForm:=TAngleForm.Create(Self);
  AssignSteprToImageMain();
  FormResolution:=TFormResolution.Create(Self);
  LoadLgSettings;
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  LgSett.Free;
  Stepr.Free;
  PropertiesForm.Free;
  AngleForm.Free;
end;

procedure AssignSteprToImageMain();
var
  pntCenter: TPoint;
begin
//������ �������� ������
  P:=GetMainVector(Form1.SpinMainAxisX.Value,Form1.SpinMainAxisY.Value,Form1.SpinMainAxisZ.Value);
  if IsNullVector(P) then exit;
//
  DrawRectAround;
  Stepr.Width:=Form1.ClientWidth-Form1.ImageMain.Left-5;
  Stepr.Height:=Form1.ClientHeight-4;
// ����������� �������� � ������ �����������
  if Stepr.Width<Stepr.Height then spScale:=Stepr.Width div 2
    else spScale:=Stepr.Height div 2;
  pntCenter.X:=Stepr.Width div 2;
  pntCenter.Y:=Stepr.Height div 2;
//
  DrawGraphics(Form1.RadioGroupBuildType.ItemIndex, pntCenter, Stepr);
  Form1.ImageMain.Canvas.Draw(2,2,Stepr);
end;

procedure DrawGraphics(BuildType: Byte; //��� ��������? �������������� ��� ����������
                       pntCenter: TPoint; //�������� ���������� ������ �������
                       var Img: TBitmap //���� �������� �����������
                       );
var
  DirTipa {����������� ���� ...}: Vector;
  i,n: Word; //����� ����������� ������, � ����� ������ �� ���� �����������
  CurTypeOfDirs: string; //����������� ������ ���������� ��� �����������
  AllTipDirs: SomeDir; //��� ����������� ������ ����, 48 ����
  N_tipdir: Byte;
begin
  dRtmp:=15;
  Img.Canvas.Font.Name:='Times New Roman';
  Img.Canvas.Font.size:=Round(7*(spScale/250));
  Rewrite(TempFile);
  ClearGraphicArea(Img.Canvas.Handle,0,0,Img.Width,Img.Height);
  if BuildType=0 then
  begin
    Img.Canvas.Pen.Color:=clBlack;
    Img.Canvas.Ellipse(pntCenter.X-spScale+Round(spScale/20),pntCenter.Y-spScale+Round(spScale/20),
                       pntCenter.X+spScale-Round(spScale/20),pntCenter.Y+spScale-Round(spScale/20));
  end;
  //DrawProjection(P,P,pntCenter,Form1.CheckBoxShowIndex.Checked,clRed,clRed,Img);
  for i:=0 to Form1.CheckListBoxAssemblage.Items.Count-1 do
    if Form1.CheckListBoxAssemblage.Checked[i]=true then
    begin
      CurTypeOfDirs:=Form1.CheckListBoxAssemblage.Items.Strings[i];
      ReadVectorFromString(CurTypeOfDirs,'[',' ',']',DirTipa);
      AllTipDirs:=TipDir2(DirTipa,N_tipdir);
      for n:=1 to N_tipdir do
        if (TestOfTop(AllTipDirs[n],P)=true)and(TestOfReflection(MaxIndex,AllTipDirs[n])=true)
        then DrawProjection(P,AllTipDirs[n],pntCenter,Form1.CheckBoxShowIndex.Checked,clRed,clRed,Img);
  end;
  DrawProjection(P,P,pntCenter,Form1.CheckBoxShowIndex.Checked,clDkGray,clDkGray,Img);
  CloseFile(TempFile);
end;


procedure DrawRectAround();
begin
  Form1.ImageMain.Canvas.Pen.Color:=clMaroon;
  Form1.ImageMain.Canvas.Rectangle(0,0,Form1.ImageMain.Width,Form1.ImageMain.Height);
  Form1.ImageMain.Canvas.Rectangle(1,1,Form1.ImageMain.Width-1,Form1.ImageMain.Height-1);
end;


procedure TForm1.ComboBoxMainAxisChange(Sender: TObject);
var
  Str: string;
begin
  Str:=ComboBoxMainAxis.Items.Strings[ComboBoxMainAxis.ItemIndex];
  SpinMainAxisX.Value:=StrToInt(Str[1]);
  SpinMainAxisY.Value:=StrToInt(Str[3]);
  SpinMainAxisZ.Value:=StrToInt(Str[5]);
  //AssignSteprToImageMain;
  AllotVecAng[1]:=0; AllotVecAng[2]:=0; AllotVecAng[3]:=0;
  AllotVecAng2[1]:=0; AllotVecAng2[2]:=0; AllotVecAng2[3]:=0;
  AllotVecOpt[1]:=0; AllotVecOpt[2]:=0; AllotVecOpt[3]:=0;
end;

procedure TForm1.SpinMainAxisXChange(Sender: TObject);
begin
  if (SpinMainAxisX.Text='')or(SpinMainAxisY.Text='')or(SpinMainAxisZ.Text='') then exit
  else AssignSteprToImageMain;  //���� � SpinEdit'�� ��� ��������, �� �����, ����� �����������
  SetNullVector(AllotVecAng);
  SetNullVector(AllotVecAng2);
  ComboBoxMainAxis.Text:='Axis';
  if not IsNullVector(AllotVecOpt) then
  begin
    if SpeedButtonModeOption.Down then SetAllotation(AllotVecOpt,clBlue,clBlue);
    if PropertiesForm.Visible then PropertiesForm.FormActivate(Self); //���� ����� �������, �� ��� �����������
  end;
end;

procedure TForm1.MainAxisOnChange(Available: Boolean);
begin
  if not Available then
  begin
    SpinMainAxisX.OnChange:=nil;
    SpinMainAxisY.OnChange:=nil;
    SpinMainAxisZ.OnChange:=nil;
  end
  else
  begin
    SpinMainAxisX.OnChange:=SpinMainAxisXChange;
    SpinMainAxisY.OnChange:=SpinMainAxisXChange;
    SpinMainAxisZ.OnChange:=SpinMainAxisXChange;
  end;
end;


procedure TForm1.SetNewAssemblage(ColFName: string; OnlyShown: Boolean);
var
  ColFile: TextFile; //���� ������, ��� ������
  i: SmallInt; //����� ����� � ����� ������, � ����� ������ ������
  strVec: string; //��� ���������� ������ �� ������
  V: Vector;
  arV: array [0..2] of Double;
  MaxIndVal,MaxInd: byte;
begin
  AssignFile(ColFile,ColFName);
  Reset(ColFile);
  for i:=0 to CheckListBoxAssemblage.Items.Count-1 do
    CheckListBoxAssemblage.Checked[i]:=false; //��������� ��������
  MaxInd:=0;
  While Not SeekEoF(ColFile) do
  begin
    ReadLn(ColFile,strVec);
    ReadVectorFromString(strVec,'[',' ',']',V);
    arV[0]:=V[1];arV[1]:=V[2];arV[2]:=V[3];
    MaxIndVal:=FloatToInt(MaxValue(arV));
    if MaxIndVal>MaxInd then MaxInd:=MaxIndVal;
  end;
  if (MaxInd>7)and(not OnlyShown) then SetMaxIndex(MaxInd);
  CloseFile(ColFile);
  Reset(ColFile);
  While Not SeekEoF(ColFile) do
  begin
    ReadLn(ColFile,strVec);
    i:=CheckListBoxAssemblage.Items.IndexOf(strVec);
    if i<>-1 then CheckListBoxAssemblage.Checked[i]:=true;
  end;
  CloseFile(ColFile);
  ColFName:=GetFileNameOnly(ColFName);
  StaticTextCurrentAssemblage.Caption:=ColFName;
end;

procedure TForm1.SetMaxIndex(MaxInd: Byte);
var
  NewVecs: ManyVectors;
  N,i: integer;
  strDir: string;
begin
  SpinEditUpToIndex.MaxValue:=MaxInd;
  NewVecs:=GetEtalonDirs(MaxInd,N);
  CheckListBoxAssemblage.Clear;
  for i:=1 to N do
  begin
    strDir:='['+FloatToStr(NewVecs[i][1])+' '+FloatToStr(NewVecs[i][2])+' '+FloatToStr(NewVecs[i][3])+']';
    CheckListBoxAssemblage.AddItem(strDir,CheckListBoxAssemblage);
  end;

  SetNewAssemblage(LgSett.ReadString('common','Default Assemblage',''),true);
end;


procedure TForm1.ButtonRefreshClick(Sender: TObject);
begin
  AssignSteprToImageMain();
  if not IsNullVector(AllotVecOpt) then SetAllotation(AllotVecOpt,clBlue,clBlue);
  if not IsNullVector(AllotVecAng) then SetAllotation(AllotVecAng,clGreen,clGreen);
  if not IsNullVector(AllotVecAng2) then SetAllotation(AllotVecAng2,clGreen,clGreen);
  if CheckBoxXYAxis.Checked then DrawXYAxises();
end;

procedure TForm1.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key=#13
    then if GetFocus=SpinMainAxisX.Handle then
           begin
             Key:=#0;
             if SpinMainAxisX.Text='' then SpinMainAxisX.Undo;
             SpinMainAxisX.SelectAll;
             ButtonRefresh.Click;
           end
           else if GetFocus=SpinMainAxisY.Handle then
                  begin
                    Key:=#0;
                    if SpinMainAxisY.Text='' then SpinMainAxisY.Undo;
                    SpinMainAxisY.SelectAll;
                    ButtonRefresh.Click;
                  end
                  else if GetFocus=SpinMainAxisZ.Handle then
                         begin
                           Key:=#0;
                           if SpinMainAxisZ.Text='' then SpinMainAxisZ.Undo;
                           SpinMainAxisZ.SelectAll;
                           ButtonRefresh.Click;
                         end
                         else if GetFocus=SpinEditRotate.Handle then
                                begin
                                  Key:=#0;
                                  if SpinEditRotate.Text='' then SpinEditRotate.Undo;
                                  SpinEditRotate.SelectAll;
                                  ButtonRefresh.Click;
                                end;
end;

procedure TForm1.SpinEditRotateExit(Sender: TObject);
begin
  //����������� ���������� ������ ������ ��� ���� TSpinEdit'��
  if (Sender=SpinEditRotate)and(SpinEditRotate.Text='') then SpinEditRotate.Undo
  else if (Sender=SpinMainAxisX)and(SpinMainAxisX.Text='') then SpinMainAxisX.Undo
       else if (Sender=SpinMainAxisY)and(SpinMainAxisY.Text='') then SpinMainAxisY.Undo
            else if (Sender=SpinMainAxisZ)and(SpinMainAxisZ.Text='') then SpinMainAxisZ.Undo;
end;

procedure TForm1.CheckBoxShowIndexClick(Sender: TObject);
begin
  ButtonRefresh.Click;
end;

procedure TForm1.PopupItemOptionsClick(Sender: TObject);
begin
  SpeedButtonModeOption.Down:=true;
  AdditionalDraw(AllotVecOpt,G);
  {if OptionForm.Visible then OptionForm.FormActivate(Self) else} PropertiesForm.Show;
end;

procedure TForm1.ImageMainMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  J:=GetCenterOfPoint(X,Y,ImageMain.Picture.Bitmap);
  G:=GetIndexOfPoint(J[1],J[2]);
  PopupAngle.Caption:='';
  if Button=mbRight then
  begin
    if (G[1]=0)and(G[2]=0)and(G[3]=0) then exit
    else
    begin
      if IsNullVector(AllotVecAng2) then PopupAngle.Caption:='Angle between ['+VecToStr(G,' ')+'] and ...'
      else
        if IsNullVector(AllotVecAng) then PopupAngle.Caption:='Angle between ['+VecToStr(AllotVecAng2,' ')+'] and ['+VecToStr(G,' ')+']'
        else PopupAngle.Caption:='Angle between ['+VecToStr(AllotVecAng,' ')+'] and ['+VecToStr(G,' ')+']';
      PopupMenuOptions.Popup(Left+ImageMain.Left+3+X,Top+ImageMain.Top+51+Y);
    end;
  end;
  if (Button=mbLeft){and(SpeedButtonModeOption.Down)}and((G[1]<>0)or(G[2]<>0)or(G[3]<>0)) then
  begin
    AdditionalDraw(AllotVecOpt,G);
    if SpeedButtonModeOption.Down then PropertiesForm.FormActivate(Self);
    if SpeedButtonModeAngle.Down then AngleForm.FormActivate(Self);
  end;

end;

procedure TForm1.PopupMakeMainClick(Sender: TObject);
begin
  MainAxisOnChange(false);
  SpinMainAxisX.Value:=StrToInt(FloatToStr(G[1]));
  SpinMainAxisY.Value:=StrToInt(FloatToStr(G[2]));
  SpinMainAxisZ.Value:=StrToInt(FloatToStr(G[3]));
  MainAxisOnChange(true);
  AssignSteprToImageMain;
  if PropertiesForm.Visible or PropertiesForm.CheckBoxSendToHiddenOpt.Checked then SetAllotation(AllotVecOpt,clBlue,clBlue);
  PropertiesForm.FormActivate(Self);
end;

procedure TForm1.SpinMainAxisXClick(Sender: TObject);
begin
  SpinMainAxisX.SelectAll;
end;

procedure TForm1.SpinMainAxisYClick(Sender: TObject);
begin
  SpinMainAxisY.SelectAll;
end;

procedure TForm1.SpinMainAxisZClick(Sender: TObject);
begin
  SpinMainAxisZ.SelectAll;
end;

procedure TForm1.SpinEditRotateClick(Sender: TObject);
begin
  SpinEditRotate.SelectAll;
end;

Procedure TForm1.GetPrinterInfo; { �������� ���������� � �������� }
begin
  PixelsX:=GetDeviceCaps(printer.Handle,LogPixelsX);
  PixelsY:=GetDeviceCaps(printer.Handle,LogPixelsY);
end;


procedure TForm1.SpeedButtonModeOptionClick(Sender: TObject);
begin
  if PageControl1.ActivePage=TabSheetMain then ButtonRefresh.SetFocus;
  if PropertiesForm.CheckBoxSendToHiddenOpt.Checked then
  begin
    TabSheetOpt.TabVisible:=true;
    PageControl1.ActivePage:=TabSheetOpt;
  end;
  if PropertiesForm.Visible then PropertiesForm.SetFocus
  else PropertiesForm.Show;
  SpeedButtonModeOption.Down:=true;
  SetAllotation(AllotVecOpt,clBlue,clBlue);
end;

procedure TForm1.PopupAngleClick(Sender: TObject);
begin
    if IsNullVector(AllotVecAng2) then
    begin
      If not IsNullVector(G) then 
      begin
        AllotVecAng2:=G;
        SetAllotation(AllotVecAng2,clGreen,clGreen);
      end;
    end
    else
    begin
      if not IsNullVector(AllotVecAng) then
      begin
        SetAllotation(AllotVecAng2,clRed,clRed);
        AllotVecAng2:=AllotVecAng;
      end;
      AllotVecAng:=G;
      SetAllotation(AllotVecAng,clGreen,clGreen);
    end;
  SpeedButtonModeAngle.Down:=true;
  if (AngleForm.CheckBoxSendToHiddenAng.Checked){and(PageControl1.ActivePage<>TabSheetHidden)} then
  begin
    if not ExistTabInPageControl('TabSheetHiddenAng',PageControl1) then
    begin
      TabSheetHiddenAng:=TTabSheet.Create(Form1.PageControl1);
      TabSheetHiddenAng.Caption:='Angle';
      TabSheetHiddenAng.Name:='TabSheetHiddenAng';
      TabSheetHiddenAng.PageControl:=Form1.PageControl1;
    end;
    PageControl1.ActivePage:=TabSheetHiddenAng;
  end;
  AngleForm.Show;
end;

procedure TForm1.SpeedButtonModeAngleClick(Sender: TObject);
begin
  if PageControl1.ActivePage=TabSheetMain then ButtonRefresh.SetFocus;
  if AngleForm.CheckBoxSendToHiddenAng.Checked then
  begin
    TabSheetAng.TabVisible:=true;
    PageControl1.ActivePage:=TabSheetAng;
  end;
  if AngleForm.Visible then AngleForm.SetFocus
  else AngleForm.Show;
  SpeedButtonModeAngle.Down:=true;
end;

procedure TForm1.PageControl1Change(Sender: TObject);
begin
  if (PageControl1.ActivePage<>TabSheetOpt)and
     (PropertiesForm.CheckBoxSendToHiddenOpt.Checked)
  then PropertiesForm.Hide;
  if (PageControl1.ActivePage<>TabSheetAng)and
     (AngleForm.CheckBoxSendToHiddenAng.Checked)
  then AngleForm.Hide;
end;

procedure TForm1.FormEndDock(Sender, Target: TObject; X, Y: Integer);
begin
  if PropertiesForm.CheckBoxSendToHiddenOpt.Checked then
  begin
    PropertiesForm.Left:=Form1.Left+Form1.PageControl1.Left+8;
    PropertiesForm.Top:=Form1.Top+76;
  end;
  if AngleForm.CheckBoxSendToHiddenAng.Checked then
  begin
    AngleForm.Left:=Form1.Left+Form1.PageControl1.Left+8;
    AngleForm.Top:=Form1.Top+76;
  end;
end;

procedure TForm1.CheckBoxUpToIndexClick(Sender: TObject);
var
  i: integer;
  strDir: string;
  Dir: Vector;
begin
  if CheckBoxUpToIndex.Checked then
    for i:=0 to CheckListBoxAssemblage.Count-1 do
    begin
      CheckListBoxAssemblage.Checked[i]:=false;
      strDir:=CheckListBoxAssemblage.Items.Strings[i];
      ReadVectorFromString(strDir,'[',' ',']',Dir);
      if (Dir[1]<=SpinEditUpToIndex.Value)and(Dir[2]<=SpinEditUpToIndex.Value)and(Dir[3]<=SpinEditUpToIndex.Value)
        then CheckListBoxAssemblage.Checked[i]:=true;
    end
  else
    SetNewAssemblage('default.col',false);
end;

function BuildRealLaue(LtoFilm: Word): TPoint;
var
  pntSteprCentr, Coor: TPoint;
  TFile, LFile: TextFile;
  strV: string;
begin
  spScale:=Round(LtoFilm*96/25.4);
  Stepr.Width:=spScale*2;
  Stepr.Height:=spScale*2;
  pntSteprCentr.X:=0;
  pntSteprCentr.Y:=0;
  DrawGraphics(Form1.RadioGroupBuildType.ItemIndex,pntSteprCentr,Stepr);

  AssignFile(TFile,ExtractFileDir(Application.ExeName)+'\refl.tmp');
  AssignFile(LFile,ExtractFileDir(Application.ExeName)+'\laue.dat');
  Reset(TFile);
  Rewrite(LFile);
  Writeln(LFile,'0 0 1; 1');
  while not EoF(TFile) do
  begin
    Readln(TFile,strV);
    ReadXYFromString(strV,'(',',',')',Coor);
    if not ((abs(Coor.X)>spScale)or(abs(Coor.Y)>spScale)or((Coor.X=1)and(Coor.Y=1))) then
      Writeln(LFile,IntToStr(Round((Coor.X-1)*25.4/96))+' '+IntToStr(-Round((Coor.Y-1)*25.4/96))+' 1; 1');
  end;
  CloseFile(TFile);
  CloseFile(LFile);

  pntSteprCentr.X:=Form1.ImageMain.Width div 2 +1;
  pntSteprCentr.Y:=Form1.ImageMain.Height div 2 +1;
  Stepr.Width:=Form1.ImageMain.Width;
  Stepr.Height:=Form1.ImageMain.Height;
  //ClearGraphicArea(Img.Canvas.Handle,0,0,Img.Width,Img.Height);
  DrawGraphics(Form1.RadioGroupBuildType.ItemIndex,pntSteprCentr,Stepr);
  Form1.ImageMain.Canvas.Draw(2,2,Stepr);
  //ShowMessage(IntToStr(spScale));
end;

procedure AdditionalDraw(D_old, D_new: Vector);
begin
//��������� ����, ����� ������ SpeedButtonModeAngle
  if Form1.SpeedButtonModeAngle.Down then
  begin
    if ((AllotVecAng2[1]<>0)or(AllotVecAng2[2]<>0)or(AllotVecAng2[3]<>0))and
       ((AllotVecAng[1]<>0)or(AllotVecAng[2]<>0)or(AllotVecAng[3]<>0))
       then
       begin
         SetAllotation(AllotVecAng2,clRed,clRed);
         AllotVecAng2:=AllotVecAng;
         AllotVecAng:=D_new;
       end
    else
      if (AllotVecAng2[1]=0)and(AllotVecAng2[2]=0)and(AllotVecAng2[3]=0)
        then AllotVecAng2:=D_new
      else AllotVecAng:=D_new;
    if not IsNullVector(AllotVecOpt)and(PropertiesForm.Visible or PropertiesForm.CheckBoxSendToHiddenOpt.Checked) then SetAllotation(AllotVecOpt,clBlue,clBlue);
    if not IsNullVector(AllotVecAng) then SetAllotation(AllotVecAng,clGreen,clGreen);
    if not IsNullVector(AllotVecAng2) then SetAllotation(AllotVecAng2,clGreen,clGreen);
  end;
//////////
//��������� ����, ����� ������ SpeedButtonModeOption
  if Form1.SpeedButtonModeOption.Down then
  begin
    if (AllotVecOpt[1]<>0)or(AllotVecOpt[2]<>0)or(AllotVecOpt[3]<>0) //���� �� ����� ��� ������� ���� ����,�� ...
      then SetAllotation(AllotVecOpt,clRed,clRed); // ...������ ���������
   AllotVecOpt:=D_new; //��������� ������ ���������
   if not IsNullVector(AllotVecAng)and(AngleForm.Visible or AngleForm.CheckBoxSendToHiddenAng.Checked) then SetAllotation(AllotVecAng,clGreen,clGreen);
   if not IsNullVector(AllotVecAng2)and(AngleForm.Visible or AngleForm.CheckBoxSendToHiddenAng.Checked) then SetAllotation(AllotVecAng2,clGreen,clGreen);
   if not IsNullVector(AllotVecOpt) then SetAllotation(AllotVecOpt,clBlue,clBlue);
  end;
//////////
end;

procedure LoadLgSettings;
begin
  if not FileExists('lgraph.ini') then
  begin
    MessageBox(Form1.Handle,'Setting file is not found','Error',MB_ICONERROR);
    exit;
  end;
  Form1.StaticTextDefaultAssemblage.Caption:=LgSett.ReadString('Common','Default assemblage','nil');
end;

procedure SaveLgSettings;
begin

end;

procedure TForm1.TabSheetOptShow(Sender: TObject);
begin
  if not PropertiesForm.Visible then PropertiesForm.Show;
  if AngleForm.Visible then AngleForm.Hide;
end;

procedure TForm1.TabSheetAngShow(Sender: TObject);
begin
  if not AngleForm.Visible then AngleForm.Show;
  if PropertiesForm.Visible then PropertiesForm.Hide;
end;

procedure TForm1.SpeedButtonModeSimpleClick(Sender: TObject);
begin
  if PageControl1.ActivePage=TabSheetMain then ButtonRefresh.SetFocus;
end;

procedure TForm1.Print1Click(Sender: TObject);
var
  PRect: TRect;
  pntCenter: TPoint;
begin
  FormPrint:=TFormPrint.Create(self);
  FormPrint.Show;
end;

procedure TForm1.DrawXYAxises();
begin
  ImageMain.Canvas.MoveTo(0, ImageMain.Height div 2);
  ImageMain.Canvas.LineTo(ImageMain.Width,ImageMain.Height div 2);
  ImageMain.Canvas.MoveTo(ImageMain.Width div 2,0);
  ImageMain.Canvas.LineTo(ImageMain.Width div 2,ImageMain.Height);
end;

procedure TForm1.CheckBoxXYAxisClick(Sender: TObject);
begin
  ButtonRefresh.Click;
end;

INITIALIZATION
  AssignFile(TempFile,GetCurrentDir+'\refl.tmp');
FINALIZATION
  Erase(TempFile);          
end.
