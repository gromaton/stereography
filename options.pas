unit options;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, Math, globtypes, ExtCtrls, Solve2,Addfunc, getangle, mxcommon;

procedure FillCombo2(Z: Vector); //���������� ComboBox2 ����� ������������� ��������
function TestOfEnabled(Dir: Vector): Boolean; //���� �� ���������
procedure CalcValues(); //���������� ������ �� ������ � ������ � ���������� ����� � ��.
function GetParam(): Extended; //�������� �������

type
  TPropertiesForm = class(TForm)
    GroupBox1: TGroupBox;
    ComboBox1: TComboBox;
    Label6: TLabel;
    ComboBox2: TComboBox;
    Label7: TLabel;
    Label8: TLabel;
    Edit1: TEdit;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label9: TLabel;
    Label1: TLabel;
    CheckBoxSendToHiddenOpt: TCheckBox;
    CheckBoxOptOnTop: TCheckBox;
    Label13: TLabel;
    Label14: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure ComboBox2Change(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
    procedure ComboBox2KeyPress(Sender: TObject; var Key: Char);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);

    procedure WMNCHitTest(var M: TWMNCHitTest); message wm_NCHitTest;
    procedure WMLButtonDown(var Message: Tmessage); message WM_NCLBUTTONDOWN;
    procedure CheckBoxSendToHiddenOptClick(Sender: TObject);
    procedure CheckBoxOptOnTopClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  PropertiesForm: TPropertiesForm;
  CurOnCaptionOpt: Boolean;
implementation

uses mainster, Types;

{$R *.DFM}
procedure TPropertiesForm.WMNCHitTest(var M: TWMNCHitTest);
begin
  inherited;
  if PropertiesForm.CheckBoxSendToHiddenOpt.Checked then
  begin
    if M.Result = htCaption then CurOnCaptionOpt:=true else CurOnCaptionOpt:=false;
  end;
end;

procedure TPropertiesForm.WMLButtonDown(var Message: Tmessage);
begin
  if PropertiesForm.CheckBoxSendToHiddenOpt.Checked then
  begin
    if CurOnCaptionOpt=false then inherited else PropertiesForm.SetFocus;
  end
  else inherited;
end;

procedure TPropertiesForm.FormActivate(Sender: TObject);
//var
  //new V: Vector;
begin
 if IsNullVector(AllotVecOpt) then AllotVecOpt:=P;
 if TestOfEnabled(AllotVecOpt)=false then Label9.Caption:='(! Forbidden !)' else Label9.Caption:='';
 Label1.Caption:='('+FloatToStr(AllotVecOpt[1])+' '+FloatToStr(AllotVecOpt[2])+' '+FloatToStr(AllotVecOpt[3])+')';
 FillCombo2(AllotVecOpt);
 CalcValues;
end;

procedure FillCombo2(Z: Vector);
var NewPol: String;
    J: Vector;
    i: Byte;
begin
 PropertiesForm.ComboBox2.Items.Clear;
 for i:=1 to 7 do begin
  J[1]:=Z[1]*i; J[2]:=Z[2]*i; J[3]:=Z[3]*i;
  if ((abs(J[1])<8)and(abs(J[2])<8)and(abs(J[3])<8))and(TestOfEnabled(J)=true) then
   begin
    NewPol:=FloatToStr(J[1])+' '+FloatToStr(J[2])+' '+FloatToStr(J[3]);
    PropertiesForm.ComboBox2.Items.Add(NewPol);
   end;
 end;
 PropertiesForm.ComboBox2.Text:=PropertiesForm.ComboBox2.Items.Strings[0];
 if PropertiesForm.ComboBox2.Text='' then PropertiesForm.ComboBox2.Text:='���';
end;

function TestOfEnabled(Dir: Vector): Boolean;
var sum: Word;
    chet: Boolean;
begin
 //Result:=true;
  sum:=Round(abs(Dir[1])+abs(Dir[2])+abs(Dir[3]));
  if (Round(Dir[1]) mod 2 =0)and(Round(Dir[2]) mod 2 =0)and(Round(Dir[3]) mod 2 =0) then chet:=true else chet:=false;
  if ((Round(Dir[1]) mod 2 <>0)and(Round(Dir[2]) mod 2 <>0)and(Round(Dir[3]) mod 2 <>0))or((sum mod 4 =0)and(chet=true))
   then Result:=true else Result:=false;
end;

procedure TPropertiesForm.ComboBox2Change(Sender: TObject);
begin
 Label1.Caption:='('+ComboBox2.Text+')';
 Label9.Caption:='';
 CalcValues;
end;

procedure CalcValues();
var LblText, Polus: String;
    i: Byte;
    Mult: Word;
    Z: Vector;
    d, DgBreg, DgZal: Extended;
begin
 i:=1; Polus:=''; LblText:=PropertiesForm.Label1.Caption;
 repeat
  i:=i+1; Polus:=Polus+LblText[i];
 until LblText[i+1]=' ';
 Z[1]:=StrToFloat(Polus);
 Polus:=''; i:=i+1;
 repeat
  i:=i+1; Polus:=Polus+LblText[i];
 until LblText[i+1]=' ';
 Z[2]:=StrToFloat(Polus);
 Polus:=''; i:=i+1;
 repeat
  i:=i+1; Polus:=Polus+LblText[i];
 until LblText[i+1]=')';
 Z[3]:=StrToFloat(Polus);

 if PropertiesForm.RadioButton1.Checked=true then Mult:=1
  else if PropertiesForm.RadioButton2.Checked=true then Mult:=10
   else Mult:=10000;
 d:=DistBtwnFlatness(GetParam,Z);
 DgBreg:=DegBragg(GetParam,StrToFloat(PropertiesForm.Edit1.Text)*Mult,Z);
 DgZal:=DegreeBtwnVectors(Z,P);
 PropertiesForm.Label11.Caption:=FloatToStr(DgBreg);
 PropertiesForm.Label12.Caption:=FloatToStr(d);
 PropertiesForm.Label10.Caption:=FloatToStr(DgZal);
end;

procedure TPropertiesForm.ComboBox1Change(Sender: TObject);
begin
 CalcValues;
end;

function GetParam(): Extended;
begin
 If PropertiesForm.ComboBox1.Text='Si' then Result:=5.428
   else if PropertiesForm.ComboBox1.Text='Ge' then Result:=5.657
   else if PropertiesForm.ComboBox1.Text='Ga As' then Result:=5.653
   else if PropertiesForm.ComboBox1.Text='Ga P'  then Result:=5.451
   else if PropertiesForm.ComboBox1.Text='In As' then Result:=6.058
   else if PropertiesForm.ComboBox1.Text='Al As' then Result:=5.662
   else if PropertiesForm.ComboBox1.Text='In P' then Result:=5.869
   else if PropertiesForm.ComboBox1.Text='Al P' then Result:=5.463
   else Result:=0;
end;

procedure TPropertiesForm.RadioButton1Click(Sender: TObject);
begin
 CalcValues;
end;

procedure TPropertiesForm.ComboBox2KeyPress(Sender: TObject; var Key: Char);
begin
 Key:=#0;
end;

procedure TPropertiesForm.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
 if Key=#13 then CalcValues;
end;

procedure TPropertiesForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if CheckBoxSendToHiddenOpt.Checked then Form1.TabSheetOpt.TabVisible:=false;
  Form1.SpeedButtonModeSimple.Down:=true;
  if not IsNullVector(AllotVecOpt) then SetAllotation(AllotVecOpt,clRed,clRed);
end;

procedure TPropertiesForm.FormCreate(Sender: TObject);
begin
  Left:=GetMaxX-Width;
  Top:=0;
  Edit1.Text:=FloatToStr(mx_StrToExt('1,54'));
end;

procedure TPropertiesForm.CheckBoxSendToHiddenOptClick(Sender: TObject);
begin
  Hide;
  if CheckBoxSendToHiddenOpt.Checked then
  begin
    Left:=Form1.Left+Form1.PageControl1.Left+8;
    Top:=Form1.Top+76;
    FormStyle:=fsStayOnTop;
    CheckBoxOptOnTop.Enabled:=false;
    Form1.TabSheetOpt.TabVisible:=true;
    Form1.PageControl1.ActivePage:=Form1.TabSheetOpt;
  end  
  else
  begin
    Left:=GetMaxX-Width;
    Top:=0;
    CheckBoxOptOnTop.Enabled:=true;
    if CheckBoxOptOnTop.Checked then FormStyle:=fsStayOnTop
    else FormStyle:=fsNormal;
    Form1.TabSheetOpt.TabVisible:=false;
  end;
  Show;
end;

procedure TPropertiesForm.CheckBoxOptOnTopClick(Sender: TObject);
begin
  if CheckBoxOptOnTop.Checked then FormStyle:=fsStayOnTop else FormStyle:=fsNormal;
end;

end.
