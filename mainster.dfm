object Form1: TForm1
  Left = 261
  Top = 86
  Width = 826
  Height = 547
  Caption = 'Stereography'
  Color = clBtnFace
  UseDockManager = True
  DragKind = dkDock
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  Menu = MainMenu1
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnEndDock = FormEndDock
  OnKeyPress = FormKeyPress
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object ImageMain: TImage
    Left = 220
    Top = 0
    Width = 441
    Height = 393
    OnMouseDown = ImageMainMouseDown
  end
  object PageControl1: TPageControl
    Left = 25
    Top = 0
    Width = 193
    Height = 401
    ActivePage = TabSheetMain
    MultiLine = True
    TabOrder = 0
    OnChange = PageControl1Change
    object TabSheetMain: TTabSheet
      Caption = 'Main'
      object GroupBoxMainAxis: TGroupBox
        Left = 0
        Top = 0
        Width = 185
        Height = 81
        Caption = 'Projection axis'
        TabOrder = 0
        object SpinMainAxisX: TSpinEdit
          Left = 8
          Top = 24
          Width = 38
          Height = 22
          MaxLength = 2
          MaxValue = 99
          MinValue = -99
          TabOrder = 0
          Value = 1
          OnChange = SpinMainAxisXChange
          OnClick = SpinMainAxisXClick
          OnExit = SpinEditRotateExit
        end
        object SpinMainAxisY: TSpinEdit
          Left = 46
          Top = 24
          Width = 38
          Height = 22
          MaxLength = 2
          MaxValue = 99
          MinValue = -99
          TabOrder = 1
          Value = 1
          OnChange = SpinMainAxisXChange
          OnClick = SpinMainAxisYClick
          OnExit = SpinEditRotateExit
        end
        object SpinMainAxisZ: TSpinEdit
          Left = 84
          Top = 24
          Width = 38
          Height = 22
          MaxLength = 2
          MaxValue = 99
          MinValue = -99
          TabOrder = 2
          Value = 1
          OnChange = SpinMainAxisXChange
          OnClick = SpinMainAxisZClick
          OnExit = SpinEditRotateExit
        end
        object ComboBoxMainAxis: TComboBox
          Left = 128
          Top = 24
          Width = 49
          Height = 21
          ItemHeight = 13
          TabOrder = 3
          Text = 'axis'
          OnChange = ComboBoxMainAxisChange
          Items.Strings = (
            '0 0 1'
            '0 1 1'
            '1 1 1')
        end
        object ButtonRefresh: TButton
          Left = 48
          Top = 53
          Width = 75
          Height = 20
          Caption = 'Refresh'
          TabOrder = 4
          OnClick = ButtonRefreshClick
        end
      end
      object GroupBoxRotate: TGroupBox
        Left = 0
        Top = 149
        Width = 185
        Height = 57
        Caption = 'Rotation'
        TabOrder = 1
        object StaticTextRotate: TStaticText
          Left = 48
          Top = 24
          Width = 75
          Height = 17
          Caption = 'Angle, degrees'
          TabOrder = 0
        end
        object SpinEditRotate: TSpinEdit
          Left = 128
          Top = 21
          Width = 49
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 1
          Value = 0
          OnClick = SpinEditRotateClick
          OnExit = SpinEditRotateExit
        end
      end
      object GroupBoxImgOpt: TGroupBox
        Left = 0
        Top = 213
        Width = 185
        Height = 68
        Caption = 'Settings'
        TabOrder = 2
        object CheckBoxShowIndex: TCheckBox
          Left = 4
          Top = 16
          Width = 137
          Height = 17
          Caption = 'Show indexes'
          TabOrder = 0
          OnClick = CheckBoxShowIndexClick
        end
        object CheckBoxFilter: TCheckBox
          Left = 4
          Top = 32
          Width = 170
          Height = 17
          Caption = 'Off forbidden reflections'
          TabOrder = 1
          OnClick = CheckBoxShowIndexClick
        end
        object CheckBoxXYAxis: TCheckBox
          Left = 4
          Top = 48
          Width = 97
          Height = 17
          Caption = 'XY Axis'
          TabOrder = 2
          OnClick = CheckBoxXYAxisClick
        end
      end
      object RadioGroupBuildType: TRadioGroup
        Left = 0
        Top = 88
        Width = 185
        Height = 57
        Caption = 'Mode'
        ItemIndex = 0
        Items.Strings = (
          'Stereographic projection'
          'Lauegram')
        TabOrder = 3
        OnClick = CheckBoxShowIndexClick
      end
    end
    object TabSheetAssemblage: TTabSheet
      Caption = 'Poles'
      ImageIndex = 1
      object Label1: TLabel
        Left = 6
        Top = 8
        Width = 82
        Height = 13
        Caption = 'Current selection:'
      end
      object Label2: TLabel
        Left = 6
        Top = 381
        Width = 82
        Height = 13
        Caption = 'Default selection:'
      end
      object CheckListBoxAssemblage: TCheckListBox
        Left = 5
        Top = 32
        Width = 172
        Height = 321
        Columns = 3
        ItemHeight = 13
        Items.Strings = (
          '[0 0 1]'
          '[0 1 1]'
          '[0 1 2]'
          '[0 1 3]'
          '[0 1 4]'
          '[0 1 5]'
          '[0 1 6]'
          '[0 1 7]'
          '[0 2 3]'
          '[0 2 5]'
          '[0 2 7]'
          '[0 3 4]'
          '[0 3 5]'
          '[0 3 7]'
          '[0 4 5]'
          '[0 4 7]'
          '[0 5 6]'
          '[0 5 7]'
          '[0 6 7]'
          '[1 1 1]'
          '[1 1 2]'
          '[1 1 3]'
          '[1 1 4]'
          '[1 1 5]'
          '[1 1 6]'
          '[1 1 7]'
          '[1 2 2]'
          '[1 2 3]'
          '[1 2 4]'
          '[1 2 5]'
          '[1 2 6]'
          '[1 2 7]'
          '[1 3 3]'
          '[1 3 4]'
          '[1 3 5]'
          '[1 3 6]'
          '[1 3 7]'
          '[1 4 4]'
          '[1 4 5]'
          '[1 4 6]'
          '[1 4 7]'
          '[1 5 5]'
          '[1 5 6]'
          '[1 5 7]'
          '[1 6 6]'
          '[1 6 7]'
          '[1 7 7]'
          '[2 2 3]'
          '[2 2 5]'
          '[2 2 7]'
          '[2 3 3]'
          '[2 3 4]'
          '[2 3 5]'
          '[2 3 6]'
          '[2 3 7]'
          '[2 4 5]'
          '[2 4 7]'
          '[2 5 5]'
          '[2 5 6]'
          '[2 5 7]'
          '[2 6 7]'
          '[2 7 7]'
          '[3 3 4]'
          '[3 3 5]'
          '[3 3 7]'
          '[3 4 4]'
          '[3 4 5]'
          '[3 4 6]'
          '[3 4 7]'
          '[3 5 5]'
          '[3 5 6]'
          '[3 5 7]'
          '[3 6 7]'
          '[3 7 7]'
          '[4 4 5]'
          '[4 4 7]'
          '[4 5 5]'
          '[4 5 6]'
          '[4 5 7]'
          '[4 6 7]'
          '[4 7 7]'
          '[5 5 6]'
          '[5 5 7]'
          '[5 6 6]'
          '[5 6 7]'
          '[5 7 7]'
          '[6 6 7]'
          '[6 7 7]')
        TabOrder = 0
      end
      object StaticTextCurrentAssemblage: TStaticText
        Left = 92
        Top = 8
        Width = 53
        Height = 17
        BevelInner = bvNone
        BevelOuter = bvNone
        Caption = 'default.col'
        TabOrder = 1
      end
      object StaticTextDefaultAssemblage: TStaticText
        Left = 93
        Top = 381
        Width = 61
        Height = 17
        BevelInner = bvNone
        BevelOuter = bvNone
        Caption = 'mostfreq.col'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = cl3DDkShadow
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        TabOrder = 2
      end
      object CheckBoxUpToIndex: TCheckBox
        Left = 0
        Top = 360
        Width = 129
        Height = 17
        Caption = 'All with indexes up to'
        TabOrder = 3
        OnClick = CheckBoxUpToIndexClick
      end
      object SpinEditUpToIndex: TSpinEdit
        Left = 128
        Top = 357
        Width = 41
        Height = 22
        MaxValue = 7
        MinValue = 0
        TabOrder = 4
        Value = 0
        OnChange = CheckBoxUpToIndexClick
      end
    end
    object TabSheetOpt: TTabSheet
      Caption = 'Properties'
      ImageIndex = 2
      TabVisible = False
      OnShow = TabSheetOptShow
    end
    object TabSheetAng: TTabSheet
      Caption = 'Angles'
      ImageIndex = 3
      TabVisible = False
      OnShow = TabSheetAngShow
    end
  end
  object PanelInstruments: TPanel
    Left = 0
    Top = 0
    Width = 25
    Height = 401
    BevelInner = bvLowered
    TabOrder = 1
    object SpeedButtonModeSimple: TSpeedButton
      Left = 2
      Top = 2
      Width = 22
      Height = 22
      Hint = 'Normal mode'
      GroupIndex = 1
      Down = True
      Flat = True
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000120B0000120B00000000000000000000E5E5E5E5E5E5
        E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5000000090909B3B3B3E3E3
        E3E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5
        E5E51212126969696A6A6A000000E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5
        E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5000000696969696969141414E5E5
        E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5000000E5E5E5E5E5E5E5E5E500
        00006868686767671E1E1EE5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5
        E5E5E5000000000000E5E5E5E5E5E50000006969696A6A6A030303E5E5E5E5E5
        E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E51414146666660C0C0C00000068
        6868686868020202E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5
        E5E5E50F0F0F6767676767676A6A6A696969666666010101E5E5E5E5E5E5E5E5
        E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E51E1E1E66666668686867676768
        6868666666393939020202020202000000E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5
        E5E5E52929296666666868686666666868686767676969696D6E6D000000E5E5
        E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E51F1F1F68686866666666666667
        6767696969686968000000E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5
        E5E5E51D1D1D676767666666676767676767676767000000E5E5E5E5E5E5E5E5
        E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E50C0C0C66666666666666666669
        6969000000E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5
        E5E5E51B1B1B6666666868686666660E0E0EE5E5E5E5E5E5E5E5E5E5E5E5E5E5
        E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E50606066666666A6B6A090909E5
        E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5
        E5E5E5040404676767000000E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5
        E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5000000000000E5E5E5E5E5E5E5
        E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5}
      ParentShowHint = False
      ShowHint = True
      OnClick = SpeedButtonModeSimpleClick
    end
    object SpeedButtonModeOption: TSpeedButton
      Left = 2
      Top = 24
      Width = 22
      Height = 22
      Hint = 'Pole selection mode with properties displaying'
      GroupIndex = 1
      Flat = True
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000120B0000120B00000000000000000000E5E5E5E5E5E5
        E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5FF3F3FFF22
        22FF1F1FFF3737FF7171E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5
        E5E5E5E5E5E5E5E5E5E5E5E5E5E5FF1515FF0C0CFA4646E5E5E5E5E5E5E5E5E5
        E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5FF13
        13FF0909F94A4AE5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5
        E5E5E5E5E5E5E5E5E5E5E5E5E5E5FF1313FF0909F94A4AE5E5E5E5E5E5E5E5E5
        E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5FF0E
        0EFF0909F94A4AE5E5E5E5E5E5E5E5E5DCDCDCB9B9B9A7A7A7A3A3A3A7A7A7B9
        B9B9DCDCDCE5E5E5E5E5E5FF7A7AFF5E5EFF5D5DF98787E5E5E5E5E5E5DCDCDC
        A3A3A3999999999999999999999999999999A3A3A3DCDCDCE5E5E5E5E5E5E5E5
        E5E5E5E5E5E5E5E5E5E5DCDCDCA3A3A399999992929285858582828285858592
        9292999999A3A2A2DECFCFE7D6D6FC3434FC3434FC3434E7D8D8B9B9B9999999
        9292928080807F7F7F7F7F7F7F7F7F808080929292999999BAB6B6FC3434FC34
        34FC3434FC3434FC3434A7A7A79999998585857F7F7F7C7C7C6969697C7C7C7F
        7F7F858585999999A7A5A5E7D8D8FC3636FC3535FC3434E7D6D6A3A3A3999999
        8282827F7F7F6969696666666969697F7F7F828282999999A3A3A3E6E3E3E8E1
        E1E9DFDFE8E0E0E6E4E4A7A7A79999998585857F7F7F7C7C7C6969697C7C7C7F
        7F7F858585999999A7A7A7E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5B9B9B9999999
        9292928080807F7F7F7F7F7F7F7F7F808080929292999999B9B9B9E5E5E5E5E5
        E5E5E5E5E5E5E5E5E5E5DCDCDCA3A3A399999992929285858582828285858592
        9292999999A3A3A3DCDCDCE5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5DCDCDC
        A3A3A3999999999999999999999999999999A3A3A3DCDCDCE5E5E5E5E5E5E5E5
        E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5DCDCDCB9B9B9A7A7A7A3A3A3A7A7A7B9
        B9B9DCDCDCE5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5}
      ParentShowHint = False
      ShowHint = True
      OnClick = SpeedButtonModeOptionClick
    end
    object SpeedButtonModeAngle: TSpeedButton
      Left = 2
      Top = 46
      Width = 22
      Height = 22
      Hint = 'Angle measurement mode with pole selection'
      GroupIndex = 1
      Flat = True
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000120B0000120B00000000000000000000E5E5E5E5E5E5
        E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5C2C2C2535353868686A4A4
        A4E3E3E3DADADAE5E5E5E5E5E5DFDFDFD0D0D0D7D7D7D7D7D7D6D6D6D5D5D5D5
        D5D5D3D3D3D2D2D2797979484848464646454545B7B7B7CACACAD3D3D3444444
        4545454545454242424444444646464444444747474646464646466565656464
        646464643D3D3D494949DCDCDC666666858585D4D4D4D7D7D7D8D8D8D4D4D4D9
        D9D9D4D4D4D9D9D97D7D7D454545484848464646A9A9A9C6C6C6E5E5E5B0B0B0
        474747DBDBDBE5E5E5E5E5E5E5E5E5E5E5E5E5E5E5BDBDBD4E4E4E888888B2B2
        B2E5E5E5E1E1E1E5E5E5E5E5E5DADADA3C3C3CBEBEBEE5E5E5E5E5E5E5E5E5E5
        E5E5E5E5E5E1E1E1E3E3E6E2E2E2E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5
        838383777777DCDCDCE5E5E5E5E5E5E5E5E5D9D9D90F0FC20707E10202F5CACA
        CADEDEE64040E40909D8E5E5E5E5E5E5CACACA353535D7D7D7E5E5E5E5E5E5E5
        E5E50606E45959F0E5E5E5CACAE91A1ADE4242EAA9A9DBE6E6E6E5E5E5E5E5E5
        DCDCDC5656569F9F9FE5E5E5E5E5E5E5E5E50808DAE5E5E5E5E5E5E5E5E5AFAF
        EA0C0CC6E5E5E5E5E5E5E5E5E5E5E5E5E5E5E59E9E9E565656DCDCDC696969DF
        DFE60505E8D2D2E8E5E5E5E5E5E5D9D9E72222D3E6E6E6E5E5E5E5E5E5E5E5E5
        E5E5E5D2D2D23535356A6A6A6B6B6BD9D9E20202F5E5E5E5E5E5E5E5E5E5E6E6
        E63434BFB9B9D7E5E5E5E5E5E5E5E5E57272727373736B6B6B696969666666DF
        DFDF0909DB9393E6E5E5E5D3D3E91010F3BABADF3838E8E5E5E5E5E5E5E5E5E5
        D9D9D9686868696969717171666666DADADAC8C8DD0808DD0909D91C1CDB8C8C
        F1E6E6E61212E8B1B1EBE5E5E5E5E5E5E5E5E5C5C5C56767674F4F4F676767DF
        DFDFE5E5E5C0C0EAE5E5E5DBDBE7E5E5E5E5E5E5B3B3EB0808FFE5E5E5E5E5E5
        E5E5E5E5E5E5D4D4D4676767666666DCDCDCE5E5E5E5E5E5E5E5E5E5E5E5E5E5
        E5E5E5E5CBCBE83A3AF9E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5D6D6D6595959D7
        D7D7E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5DDDDE6D6D6E7}
      ParentShowHint = False
      ShowHint = True
      OnClick = SpeedButtonModeAngleClick
    end
  end
  object MainMenu1: TMainMenu
    Left = 280
    Top = 8
    object File1: TMenuItem
      Caption = '&File'
      object Open1: TMenuItem
        Caption = 'Open pole selection...'
        ShortCut = 16463
        OnClick = Open1Click
      end
      object SaveAssemblageAs1: TMenuItem
        Caption = 'Save current selection...'
        ShortCut = 16449
        OnClick = SaveAssemblageAs1Click
      end
      object MainMenuSaveImageAs: TMenuItem
        Caption = 'Save image...'
        ShortCut = 16467
        OnClick = MainMenuSaveImageAsClick
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object Print1: TMenuItem
        Caption = 'Print...'
        ShortCut = 16464
        OnClick = Print1Click
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Exit1: TMenuItem
        Caption = 'Exit'
        OnClick = Exit1Click
      end
    end
    object Edit1: TMenuItem
      Caption = '&Service'
      object N3: TMenuItem
        Caption = 'Save current reflex set as default'
        OnClick = N3Click
      end
      object N4: TMenuItem
        Caption = 'Set max available reflex index ...'
        OnClick = N4Click
      end
    end
    object Window1: TMenuItem
      Caption = '&Window'
      object MainMenuRefresh: TMenuItem
        Caption = 'Refresh'
        ShortCut = 116
        OnClick = MainMenuRefreshClick
      end
    end
  end
  object SaveSteprDialog: TSaveDialog
    Filter = 'JPEG Image Files (*.jpg)|*.jpg|Bitmaps (*.bmp)|*.bmp'
    Left = 312
    Top = 8
  end
  object PopupMenuOptions: TPopupMenu
    Left = 344
    Top = 8
    object PopupMakeMain: TMenuItem
      Caption = 'Build its projection'
      OnClick = PopupMakeMainClick
    end
    object PopupAngle: TMenuItem
      Caption = 'Angle between [...] and [...]'
      OnClick = PopupAngleClick
    end
    object PopupItemOptions: TMenuItem
      Caption = 'Pole properties'
      OnClick = PopupItemOptionsClick
    end
  end
  object SaveAssemblageDialog: TSaveDialog
    Filter = 'Assemblage (*.col)|*.col'
    Title = 'Save current pole selection as ...'
    Left = 408
    Top = 8
  end
  object OpenAssemblageDialog: TOpenDialog
    Filter = 'Assemblage (*.col)|*.col'
    Title = 'Open pole selection file ...'
    Left = 376
    Top = 8
  end
  object PrinterSetupDialog1: TPrinterSetupDialog
    Left = 440
    Top = 8
  end
end
