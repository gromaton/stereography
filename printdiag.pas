unit printdiag;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Printers, StdCtrls,
  globtypes;

type
  TFormPrint = class(TForm)
    ImagePreview: TImage;
    ButtonParam: TButton;
    ButtonPrint: TButton;
    procedure FormCreate(Sender: TObject);
    procedure ButtonParamClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure ImagePreviewResize;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ButtonPrintClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormPrint: TFormPrint;
  AspectRatio: Extended;

implementation

uses mainster;

{$R *.dfm}

procedure TFormPrint.FormCreate(Sender: TObject);
begin
  AspectRatio:=Printer.PageWidth/Printer.PageHeight;
  ImagePreview.Width:=434;
  PatBlt(ImagePreview.Canvas.Handle, 0, 0, ImagePreview.Width, ImagePreview.Height, WHITENESS);
  ImagePreviewResize;
end;

procedure TFormPrint.ButtonParamClick(Sender: TObject);
begin
  if Form1.PrinterSetupDialog1.Execute then
  begin
    ImagePreviewResize;
  end;
  FormActivate(Self);
end;

procedure TFormPrint.FormActivate(Sender: TObject);
var
  pntCenter: TPoint;
  PRect: TRect;
begin
  //PatBlt(ImagePreview.Canvas.Handle, 0, 0, ImagePreview.Width, ImagePreview.Height, WHITENESS);
  Stepr.Width:=ImagePreview.Width-2;
  Stepr.Height:=ImagePreview.Height-2;
  if Stepr.Width<Stepr.Height then spScale:=Stepr.Width div 2
  else spScale:=Stepr.Height div 2;
  pntCenter.X:=Stepr.Width div 2;
  pntCenter.Y:=Stepr.Height div 2;
  DrawGraphics(Form1.RadioGroupBuildType.ItemIndex, pntCenter, Stepr);
  ImagePreview.Canvas.Rectangle(ImagePreview.ClientRect);
  ImagePreview.Canvas.Draw(1,1,Stepr);
  //ImagePreview.Canvas.Rectangle(ImagePreview.ClientRect);
end;

procedure TFormPrint.ImagePreviewResize;
begin
  if Printer.Orientation=poPortrait then
      with ImagePreview do begin
        Left:=96; Top:=8;
        Width:=301; Height:=434;
      end
    else
      with ImagePreview do begin
        Left:=30; Top:=80;
        Width:=434; Height:=301;
      end;
end;


procedure TFormPrint.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Form1.ButtonRefresh.Click;
end;

procedure TFormPrint.ButtonPrintClick(Sender: TObject);
var
  pntCenter: TPoint;
  PRect: TRect;
begin
  Stepr.Width:=Printer.PageWidth div 2;
  Stepr.Height:=Printer.PageHeight div 2;
  if Stepr.Width<Stepr.Height then spScale:=Stepr.Width div 2
  else spScale:=Stepr.Height div 2;
  pntCenter.X:=Stepr.Width div 2;
  pntCenter.Y:=Stepr.Height div 2;
  DrawGraphics(Form1.RadioGroupBuildType.ItemIndex, pntCenter, Stepr);
  PRect.Left:=0; PRect.Top:=0;
  PRect.Right:=Printer.PageWidth; PRect.Bottom:=Printer.PageHeight;
  Printer.BeginDoc;
  Printer.Canvas.StretchDraw(PRect,Stepr);
  Printer.EndDoc;
  Close;
end;

end.
